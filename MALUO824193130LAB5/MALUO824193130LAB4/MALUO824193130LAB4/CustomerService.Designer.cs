﻿namespace MALUO824193130LAB4
{
    partial class CustomerService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mnuForm1 = new System.Windows.Forms.MenuStrip();
            this.startToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerBillingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewAllInfosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuForm1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuForm1
            // 
            this.mnuForm1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startToolStripMenuItem,
            this.viewAllInfosToolStripMenuItem});
            this.mnuForm1.Location = new System.Drawing.Point(0, 0);
            this.mnuForm1.Name = "mnuForm1";
            this.mnuForm1.Size = new System.Drawing.Size(674, 24);
            this.mnuForm1.TabIndex = 1;
            this.mnuForm1.Text = "menuStrip1";
            // 
            // startToolStripMenuItem
            // 
            this.startToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerInfoToolStripMenuItem,
            this.customerOrderToolStripMenuItem,
            this.customerBillingToolStripMenuItem});
            this.startToolStripMenuItem.Name = "startToolStripMenuItem";
            this.startToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F1)));
            this.startToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.startToolStripMenuItem.Text = "Start";
            // 
            // customerInfoToolStripMenuItem
            // 
            this.customerInfoToolStripMenuItem.Name = "customerInfoToolStripMenuItem";
            this.customerInfoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D1)));
            this.customerInfoToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.customerInfoToolStripMenuItem.Text = "Customer Info";
            this.customerInfoToolStripMenuItem.Click += new System.EventHandler(this.customerInfoToolStripMenuItem_Click);
            // 
            // customerOrderToolStripMenuItem
            // 
            this.customerOrderToolStripMenuItem.Name = "customerOrderToolStripMenuItem";
            this.customerOrderToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D2)));
            this.customerOrderToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.customerOrderToolStripMenuItem.Text = "Customer Order";
            this.customerOrderToolStripMenuItem.Click += new System.EventHandler(this.customerOrderToolStripMenuItem_Click);
            // 
            // customerBillingToolStripMenuItem
            // 
            this.customerBillingToolStripMenuItem.Name = "customerBillingToolStripMenuItem";
            this.customerBillingToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D3)));
            this.customerBillingToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.customerBillingToolStripMenuItem.Text = "Customer Billing";
            this.customerBillingToolStripMenuItem.Click += new System.EventHandler(this.customerBillingToolStripMenuItem_Click);
            // 
            // viewAllInfosToolStripMenuItem
            // 
            this.viewAllInfosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.horizontalToolStripMenuItem,
            this.verticalToolStripMenuItem});
            this.viewAllInfosToolStripMenuItem.Name = "viewAllInfosToolStripMenuItem";
            this.viewAllInfosToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F2)));
            this.viewAllInfosToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.viewAllInfosToolStripMenuItem.Text = "View All Infos";
            // 
            // horizontalToolStripMenuItem
            // 
            this.horizontalToolStripMenuItem.Name = "horizontalToolStripMenuItem";
            this.horizontalToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F2)));
            this.horizontalToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.horizontalToolStripMenuItem.Text = "Horizontal";
            this.horizontalToolStripMenuItem.Click += new System.EventHandler(this.horizontalToolStripMenuItem_Click);
            // 
            // verticalToolStripMenuItem
            // 
            this.verticalToolStripMenuItem.Name = "verticalToolStripMenuItem";
            this.verticalToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F3)));
            this.verticalToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.verticalToolStripMenuItem.Text = "Vertical";
            this.verticalToolStripMenuItem.Click += new System.EventHandler(this.verticalToolStripMenuItem_Click);
            // 
            // CustomerService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 432);
            this.Controls.Add(this.mnuForm1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mnuForm1;
            this.Name = "CustomerService";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MALUO824193130Customer Service Application";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.mnuForm1.ResumeLayout(false);
            this.mnuForm1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuForm1;
        private System.Windows.Forms.ToolStripMenuItem startToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerBillingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewAllInfosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verticalToolStripMenuItem;
    }
}

