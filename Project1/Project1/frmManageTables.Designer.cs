﻿namespace Project1
{
    partial class frmManageTables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMeans = new System.Windows.Forms.Label();
            this.lblTarget = new System.Windows.Forms.Label();
            this.cbo_Table_Method = new System.Windows.Forms.ComboBox();
            this.txt_Target = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.dgv_tables = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_tables)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMeans
            // 
            this.lblMeans.AutoSize = true;
            this.lblMeans.BackColor = System.Drawing.Color.Transparent;
            this.lblMeans.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMeans.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblMeans.Location = new System.Drawing.Point(170, 294);
            this.lblMeans.Name = "lblMeans";
            this.lblMeans.Size = new System.Drawing.Size(84, 18);
            this.lblMeans.TabIndex = 11;
            this.lblMeans.Text = "&Search By :";
            // 
            // lblTarget
            // 
            this.lblTarget.AutoSize = true;
            this.lblTarget.BackColor = System.Drawing.Color.Transparent;
            this.lblTarget.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTarget.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblTarget.Location = new System.Drawing.Point(170, 377);
            this.lblTarget.Name = "lblTarget";
            this.lblTarget.Size = new System.Drawing.Size(58, 18);
            this.lblTarget.TabIndex = 12;
            this.lblTarget.Text = "&Target :";
            // 
            // cbo_Table_Method
            // 
            this.cbo_Table_Method.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Table_Method.FormattingEnabled = true;
            this.cbo_Table_Method.Items.AddRange(new object[] {
            "Username",
            "User Id",
            "All"});
            this.cbo_Table_Method.Location = new System.Drawing.Point(267, 294);
            this.cbo_Table_Method.Name = "cbo_Table_Method";
            this.cbo_Table_Method.Size = new System.Drawing.Size(121, 21);
            this.cbo_Table_Method.TabIndex = 13;
            // 
            // txt_Target
            // 
            this.txt_Target.Location = new System.Drawing.Point(267, 377);
            this.txt_Target.Name = "txt_Target";
            this.txt_Target.Size = new System.Drawing.Size(121, 20);
            this.txt_Target.TabIndex = 14;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(453, 321);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 58);
            this.btnSearch.TabIndex = 15;
            this.btnSearch.Text = "&Perform Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Location = new System.Drawing.Point(583, 321);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 58);
            this.btnRemove.TabIndex = 16;
            this.btnRemove.Text = "&Cancel Booking";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // dgv_tables
            // 
            this.dgv_tables.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_tables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_tables.Location = new System.Drawing.Point(99, 40);
            this.dgv_tables.Name = "dgv_tables";
            this.dgv_tables.Size = new System.Drawing.Size(639, 198);
            this.dgv_tables.TabIndex = 17;
            // 
            // frmManageTables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImage = global::Project1.Properties.Resources.image10;
            this.ClientSize = new System.Drawing.Size(835, 442);
            this.Controls.Add(this.dgv_tables);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txt_Target);
            this.Controls.Add(this.cbo_Table_Method);
            this.Controls.Add(this.lblTarget);
            this.Controls.Add(this.lblMeans);
            this.Name = "frmManageTables";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manage Tables";
            this.Load += new System.EventHandler(this.frmManageTables_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_tables)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMeans;
        private System.Windows.Forms.Label lblTarget;
        private System.Windows.Forms.ComboBox cbo_Table_Method;
        private System.Windows.Forms.TextBox txt_Target;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.DataGridView dgv_tables;
    }
}