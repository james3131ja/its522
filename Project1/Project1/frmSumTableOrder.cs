﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1
{
    public partial class frmSumTableOrder : Form
    {
        DatabaseConnection conn = null;
        public frmSumTableOrder()
        {
            InitializeComponent();
            conn = new DatabaseConnection();
        }

        private void frmSumTableOrder_Load(object sender, EventArgs e)
        {
            lblName.Text = (SessionImpl.user != null) ? SessionImpl.user.UserName : "TBD";
            lblID.Text = (SessionImpl.user != null) ? SessionImpl.user.UserId.ToString() : "TBD";
            try
            {
                string strSeat = getTableString() + " where user_id = '" + SessionImpl.user.UserId + "'";
                //MessageBox.Show(SessionImpl.user.UserName);
                conn.loadDataGridView(dgvTableOrders, strSeat);
            }
            catch { }
           
        }

        private string getTableString()
        {
            return "select user_id,uname as User_Name,table_num,book_day,num_of_seat as book_seats," +
            "decode(timeslot,'0','morning','1','afternoon','2','evening') as section," +
            "decode(share_mode,'0','not shared','1','shared') as shared from food_tableorder join food_users using(user_id)";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
