﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MALUO824193130LAB4
{
    public class Order : Customer
    {
        private double total;
       
        public double Total { get { return total;} set { total=value;} }
        public Order() {
        Total=0;
        }
        public Order(double total) {
            Total = total;
        }
        public Order(double total,string f,string l):base(f,l)
        {
            //base.Fname =f; base.Lname=l;
            Total = total;
        }
    }
}
