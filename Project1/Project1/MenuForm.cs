﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1
{
    public class MenuForm
    {
     
        public SortedDictionary<int, Menu> menuList = null;
        
        ListView.ColumnHeaderCollection header = null;
        
        ColumnHeader[] headArray = null;
        
        public MenuForm() {
            menuList = new SortedDictionary<int, Menu>();
            preMenuList();
        }
        
        /*load menu list to listview and return the listview to menu form*/
        public ListView loadMenuToList(ListView  lst) {
            lst=preListViewHeader(lst);
            lst.Clear();
            lst.Columns.AddRange(headArray);
            foreach (Menu o in menuList.Values)
            {
                //subitem[0] is the item_id
                ListViewItem i = new ListViewItem(o.Item_id.ToString());
                i.SubItems.Add(o.Name);
                i.SubItems.Add("$ " + o.UnitPrice.ToString());
                i.SubItems.Add(o.Quantity.ToString());
                lst.Items.Add(i);
            }
            return lst;
        }
        
        /*add checked items associated with the user in database*/
        public void MenuChecks(ListView lst) {
            OrderDBDaoImpl dbOrder = new OrderDBDaoImpl();
            Order o = null;
            foreach (ListViewItem i in lst.CheckedItems)
            {
                o = new Order(SessionImpl.user.UserId, Convert.ToInt32(i.Text), Convert.ToInt32(i.SubItems[3].Text));
                dbOrder.addOrder(o);
                i.Checked = false;
            }
        }
        
        /*show item description*/
        public void showDesc(ListView lst) {
            Menu menu = null;
            try
            {
                int i = lst.SelectedIndices[0];
                menuList.TryGetValue(i, out menu);
                MessageBox.Show(menu.Name+"\n"+menu.Desc);
            }
            catch { }
        }
        

        /*increase the quantity of one item,update and return the listview as well*/
        public ListView quantityIncrease(ListView lst) {
            //num(id) of rows
            int i = 0;
            Menu menu = null;
            try
            {
                i = lst.SelectedIndices[0];
                menuList.TryGetValue(i, out menu);
                menu.Quantity += 1;
                menuList.Remove(i);
                menuList.Add(i, menu);
            }
            catch { }
            lst=loadMenuToList(lst);
            lst.Items[i].Selected = true;
            lst.Select();
            return lst;
        }

        /*decrease the quantity of one item,update and return the listview as well*/
        public ListView quantityDecrease(ListView lst) {
            Menu menu = null;
            int i = 0;
            try
            {
                i = lst.SelectedIndices[0];
                menuList.TryGetValue(i, out menu);
                menuList.Remove(i);
                menu.Quantity = (
                    ((Convert.ToInt32(menu.Quantity) - 1) < 0) ? 0 : Convert.ToInt32(menu.Quantity) - 1
                    );
                menuList.Add(i, menu);
            }
            catch { }
            lst=loadMenuToList(lst);
            lst.Items[i].Selected = true;
            lst.Select();
            return lst;
        }
        
        /*load itmes menu to the listview by category*/
        public ListView loadByCategory(ListView lst,RadioButton rdo) {
                OrderDBDaoImpl dbo = new OrderDBDaoImpl();
                string tmp = rdo.Text.Substring(1, rdo.Text.Length - 1);
                string sql = "select  * from food_menu";
                sql += (tmp.ToLower()=="All".ToLower()) ? "" : " where food_category='" + tmp + "'";
               // MessageBox.Show(tmp);
                menuList = dbo.loadMenusToList(sql);
                lst=loadMenuToList(lst);
                return lst;
        }

        /*get the original size of the header in the designer form*/
        private ListView preListViewHeader(ListView  lst)
        {
            header = lst.Columns;
            int i = 0;
            headArray = new ColumnHeader[header.Count];
            do
            {
                headArray[i] = header[i];
                i++;
            }
            while (i < header.Count);
            lst.Columns.Clear();
            return lst;
        }

        private void preMenuList()
        {
            OrderDBDaoImpl dbo = new OrderDBDaoImpl();
            string sql = "select * from food_menu";
            menuList = dbo.loadMenusToList(sql);
        }
    }
}
