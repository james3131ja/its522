﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    interface ITransfer
    {
        double deposite(Account acc,double num);
        double withdraw(Account acc, double num);
        void TransitState(Account acc);
    }
}
