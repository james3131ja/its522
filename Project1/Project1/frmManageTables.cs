﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1
{


    public partial class frmManageTables : Form
    {
        DatabaseConnection conn = null;

        public frmManageTables()
        {
            InitializeComponent();
            conn = new DatabaseConnection();
            cbo_Table_Method.SelectedIndex = 2;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                string strDelete = "Delete from food_tableorder where table_order_id ='" + dgv_tables.SelectedRows[0].Cells[0].Value.ToString() + "'";
                int i = conn.loadDataGridView(dgv_tables, strDelete) ;
                if (i == 1) { MessageBox.Show("Table Booking" + dgv_tables.CurrentRow.Cells[0].Value.ToString() + "Cancelled"); }
            }
            catch{  }
            loadTables();
        }

        private void frmManageTables_Load(object sender, EventArgs e)
        {
         loadTables();  
        }

       
        private void btnSearch_Click(object sender, EventArgs e)
        {
            switch (cbo_Table_Method.SelectedIndex)
            {
                case 1:
                    string strSearch = getTableString()+" where user_id = '" + txt_Target.Text + "'";
                    conn.loadDataGridView(dgv_tables, strSearch);
                    txt_Target.Text = "";
                    break;

                case 0:
                    string strSea = getTableString()+" where uname = '" + txt_Target.Text + "'";
                    conn.loadDataGridView(dgv_tables, strSea);
                    txt_Target.Text = "";
                    break;

                case 2:
                    loadTables();
                    txt_Target.Text = "";
                    break;
            }
        }

        private void loadTables(){
         conn.loadDataGridView(dgv_tables, getTableString());
        }


        /*set and return a common part of sql string since it is a little long*/
        private string getTableString() {
            return "select table_order_id as id,user_id,uname as User_Name,table_num,book_day,num_of_seat as book_seats,(limit_seats - num_of_seat) as seat_allowed,"+
            "decode(timeslot,'0','morning','1','afternoon','2','evening') as section,"+
            "decode(share_mode,'0','not shared','1','shared') as shared from food_tableorder join food_users using(user_id)";
        }
    }
}