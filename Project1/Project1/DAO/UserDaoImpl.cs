﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using System.Windows.Forms;

namespace Project1
{
    public class UserDaoImpl : UserDao
    {
        
        public bool compareUserToSession(User u) {
            return SessionImpl.user.Equals(u);
        }
       
        public int addUser(User u) {
            DatabaseDaoImpl dbo = new DatabaseDaoImpl();
            OracleConnection conn = dbo.data_Conn();
            int check = 0;
            string sql = "insert into food_users (uname,passwd) values ('"+u.UserName+"','"+u.UserPassword+"')";
            try
            {
                OracleCommand cmd = new OracleCommand(sql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                //MessageBox.Show("You are successfully Registered!!!");
                check = 1;
            }
            catch (OracleException ex) { //MessageBox.Show(ex.Message); 
               
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        public int getUserID(User u) {
            DatabaseDaoImpl dbo = new DatabaseDaoImpl();
            OracleConnection conn = dbo.data_Conn();
            int id = 0;
            string sql = "select user_id from food_users where uname='"+u.UserName+"'";
            try
            {
                OracleCommand cmd = new OracleCommand(sql, conn);
                conn.Open();
                OracleDataReader reader = cmd.ExecuteReader();
                reader.Read();
                id = (reader.IsDBNull(0)) ? 0 : reader.GetInt32(0);
            }
            catch (OracleException ex) { //MessageBox.Show(ex.Message); 
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return id;
        }

        public string getUserPasswd(User u) {
            DatabaseDaoImpl dbo = new DatabaseDaoImpl();
            OracleConnection conn = dbo.data_Conn();
            string pass = null;
            string sql = "select passwd from food_users where uname='" + u.UserName + "'";
            try
            {
                OracleCommand cmd = new OracleCommand(sql, conn);
                conn.Open();
                OracleDataReader reader = cmd.ExecuteReader();
                reader.Read();
                pass = (reader.IsDBNull(0)) ? null : reader.GetString(0);
            }
            catch (OracleException ex)
            { //MessageBox.Show(ex.Message); 
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return pass;
        }

        public int changePassword(User u) {
            DatabaseDaoImpl dbo = new DatabaseDaoImpl();
            OracleConnection conn = dbo.data_Conn();
            int check = 0;
            string sql = "update food_users set passwd = '" + u.UserPassword + "' where uname='" + u.UserName + "'";
                try
                {
                    OracleCommand cmd = new OracleCommand(sql, conn);
                    conn.Open();
                    check = cmd.ExecuteNonQuery();
                }
                catch (OracleException ex)
                { //MessageBox.Show(ex.Message); 
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            return check;
        }

    }
}
