﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class frmAccount : Form
    {
        Account account = null;
        
        public frmAccount()
        {
            InitializeComponent();
            account = new Account();
            account.Balance = 10000;
            account.AccountContext = new NormalState();
            lblSetState.Text = Convert.ToString(account.AccountContext);
            lsvRecords.Clear();
            lsvRecords.Columns.Add("Operation");
            lsvRecords.Columns.Add("Amount");
            lsvRecords.Columns.Add("Balance");

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cboChoice.Items.AddRange(new string[] { "With Draw", "Deposite" });
            cboChoice.SelectedIndex = 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            account.Transfer(cboChoice.SelectedIndex,Convert.ToDouble(nupAccount.Value));
            ListViewItem i = new ListViewItem(cboChoice.SelectedItem.ToString());
            i.SubItems.Add("$ " +nupAccount.Value.ToString());
            i.SubItems.Add("$ "+ account.Balance.ToString());
            lsvRecords.Items.Add(i);
            lblSetState.Text = account.AccountContext.ToString();
        }
    
    }
}
