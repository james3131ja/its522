﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    interface UserDao
    {
        bool compareUserToSession(User u);
        int addUser(User u);
        int getUserID(User u);
    }
}
