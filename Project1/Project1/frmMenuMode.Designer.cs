﻿namespace Project1
{
    partial class frmMenuMode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstMenuMode = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnCheck = new System.Windows.Forms.Button();
            this.btnIncrease = new System.Windows.Forms.Button();
            this.btnDecrease = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.rdoAppetizer = new System.Windows.Forms.RadioButton();
            this.rdoBeverage = new System.Windows.Forms.RadioButton();
            this.rdoDessert = new System.Windows.Forms.RadioButton();
            this.rdoAll = new System.Windows.Forms.RadioButton();
            this.rdoMain = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // lstMenuMode
            // 
            this.lstMenuMode.BackColor = System.Drawing.Color.BurlyWood;
            this.lstMenuMode.CheckBoxes = true;
            this.lstMenuMode.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lstMenuMode.FullRowSelect = true;
            this.lstMenuMode.GridLines = true;
            this.lstMenuMode.LabelWrap = false;
            this.lstMenuMode.Location = new System.Drawing.Point(110, 183);
            this.lstMenuMode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lstMenuMode.Name = "lstMenuMode";
            this.lstMenuMode.Size = new System.Drawing.Size(293, 267);
            this.lstMenuMode.TabIndex = 0;
            this.lstMenuMode.UseCompatibleStateImageBehavior = false;
            this.lstMenuMode.View = System.Windows.Forms.View.Details;
            this.lstMenuMode.Click += new System.EventHandler(this.lstMenuMode_Click);
            this.lstMenuMode.DoubleClick += new System.EventHandler(this.lstMenuMode_DoubleClick);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "";
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 86;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "UnitPrice";
            this.columnHeader2.Width = 77;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Quantity";
            this.columnHeader3.Width = 77;
            // 
            // btnCheck
            // 
            this.btnCheck.BackColor = System.Drawing.Color.BurlyWood;
            this.btnCheck.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCheck.Location = new System.Drawing.Point(111, 523);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(115, 32);
            this.btnCheck.TabIndex = 4;
            this.btnCheck.Text = "&Add To Cart";
            this.btnCheck.UseVisualStyleBackColor = false;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // btnIncrease
            // 
            this.btnIncrease.BackColor = System.Drawing.Color.BurlyWood;
            this.btnIncrease.Location = new System.Drawing.Point(111, 480);
            this.btnIncrease.Name = "btnIncrease";
            this.btnIncrease.Size = new System.Drawing.Size(116, 37);
            this.btnIncrease.TabIndex = 5;
            this.btnIncrease.Text = "Quantity &+";
            this.btnIncrease.UseVisualStyleBackColor = false;
            this.btnIncrease.Click += new System.EventHandler(this.btnIncrease_Click);
            // 
            // btnDecrease
            // 
            this.btnDecrease.BackColor = System.Drawing.Color.BurlyWood;
            this.btnDecrease.Location = new System.Drawing.Point(287, 480);
            this.btnDecrease.Name = "btnDecrease";
            this.btnDecrease.Size = new System.Drawing.Size(116, 37);
            this.btnDecrease.TabIndex = 6;
            this.btnDecrease.Text = "Quantity &-";
            this.btnDecrease.UseVisualStyleBackColor = false;
            this.btnDecrease.Click += new System.EventHandler(this.btnDecrease_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.BurlyWood;
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnBack.Location = new System.Drawing.Point(287, 523);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(116, 32);
            this.btnBack.TabIndex = 7;
            this.btnBack.Text = "&Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // rdoAppetizer
            // 
            this.rdoAppetizer.AutoSize = true;
            this.rdoAppetizer.BackColor = System.Drawing.Color.Transparent;
            this.rdoAppetizer.Location = new System.Drawing.Point(157, 130);
            this.rdoAppetizer.Name = "rdoAppetizer";
            this.rdoAppetizer.Size = new System.Drawing.Size(95, 24);
            this.rdoAppetizer.TabIndex = 8;
            this.rdoAppetizer.TabStop = true;
            this.rdoAppetizer.Text = "&Appetizer";
            this.rdoAppetizer.UseVisualStyleBackColor = false;
            this.rdoAppetizer.CheckedChanged += new System.EventHandler(this.rdoAppetizer_CheckedChanged);
            // 
            // rdoBeverage
            // 
            this.rdoBeverage.AutoSize = true;
            this.rdoBeverage.BackColor = System.Drawing.Color.Transparent;
            this.rdoBeverage.Location = new System.Drawing.Point(258, 130);
            this.rdoBeverage.Name = "rdoBeverage";
            this.rdoBeverage.Size = new System.Drawing.Size(103, 24);
            this.rdoBeverage.TabIndex = 9;
            this.rdoBeverage.TabStop = true;
            this.rdoBeverage.Text = "&Beverages";
            this.rdoBeverage.UseVisualStyleBackColor = false;
            this.rdoBeverage.CheckedChanged += new System.EventHandler(this.rdoBeverages_CheckedChanged);
            // 
            // rdoDessert
            // 
            this.rdoDessert.AutoSize = true;
            this.rdoDessert.BackColor = System.Drawing.Color.Transparent;
            this.rdoDessert.Location = new System.Drawing.Point(359, 130);
            this.rdoDessert.Name = "rdoDessert";
            this.rdoDessert.Size = new System.Drawing.Size(91, 24);
            this.rdoDessert.TabIndex = 10;
            this.rdoDessert.TabStop = true;
            this.rdoDessert.Text = "&Desserts";
            this.rdoDessert.UseVisualStyleBackColor = false;
            this.rdoDessert.CheckedChanged += new System.EventHandler(this.rdoDesserts_CheckedChanged);
            // 
            // rdoAll
            // 
            this.rdoAll.AutoSize = true;
            this.rdoAll.BackColor = System.Drawing.Color.Transparent;
            this.rdoAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.rdoAll.Location = new System.Drawing.Point(229, 100);
            this.rdoAll.Name = "rdoAll";
            this.rdoAll.Size = new System.Drawing.Size(44, 24);
            this.rdoAll.TabIndex = 11;
            this.rdoAll.TabStop = true;
            this.rdoAll.Text = "&All";
            this.rdoAll.UseVisualStyleBackColor = false;
            this.rdoAll.CheckedChanged += new System.EventHandler(this.rdoAll_CheckedChanged);
            // 
            // rdoMain
            // 
            this.rdoMain.AutoSize = true;
            this.rdoMain.BackColor = System.Drawing.Color.Transparent;
            this.rdoMain.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.rdoMain.Location = new System.Drawing.Point(90, 130);
            this.rdoMain.Name = "rdoMain";
            this.rdoMain.Size = new System.Drawing.Size(69, 24);
            this.rdoMain.TabIndex = 12;
            this.rdoMain.TabStop = true;
            this.rdoMain.Text = "&Mains";
            this.rdoMain.UseVisualStyleBackColor = false;
            this.rdoMain.CheckedChanged += new System.EventHandler(this.rdoMains_CheckedChanged);
            // 
            // frmMenuMode
            // 
            this.AcceptButton = this.btnCheck;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImage = global::Project1.Properties.Resources.image4;
            this.CancelButton = this.btnBack;
            this.ClientSize = new System.Drawing.Size(752, 661);
            this.Controls.Add(this.rdoMain);
            this.Controls.Add(this.rdoAll);
            this.Controls.Add(this.rdoDessert);
            this.Controls.Add(this.rdoBeverage);
            this.Controls.Add(this.rdoAppetizer);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnDecrease);
            this.Controls.Add(this.btnIncrease);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.lstMenuMode);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmMenuMode";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.frmMenuMode_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lstMenuMode;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Button btnIncrease;
        private System.Windows.Forms.Button btnDecrease;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.RadioButton rdoAppetizer;
        private System.Windows.Forms.RadioButton rdoBeverage;
        private System.Windows.Forms.RadioButton rdoDessert;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.RadioButton rdoAll;
        private System.Windows.Forms.RadioButton rdoMain;
    }
}