﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1
{
    public partial class frmOrderForm : Form
    {
        static int count_image_index = 0;
        ListView.ColumnHeaderCollection header = null;
        ColumnHeader[] headArray = null;
        String[] combo = null;
        OrdersForm orders = new OrdersForm();

        public frmOrderForm()
        {
            InitializeComponent();
            preListViewHeader();
            lstOrders.CheckBoxes = true;
            lstOrders.FullRowSelect = true;
            hideTalbeInfo();
            load_ListToView();
        }

        //prepare for the list view header here.
        private void preListViewHeader()
        {
            header = lstOrders.Columns;
            int i = 0;
            headArray = new ColumnHeader[header.Count];
            do
            {
                headArray[i] = header[i];
                i++;
            }
            while (i < header.Count);
            lstOrders.Columns.Clear();
        }

        /*get data from database to the list and load it to the listview*/
        private void load_ListToView()
        {
            lstOrders.Clear();
            lstOrders.Columns.AddRange(headArray);
            lstOrders = orders.loadListToView(lstOrders);
            lblAmount.Text = orders.CalculateOrderTotal().ToString();
        }
       
        //hide the table if user do not wanna to book a seat
        private void hideTalbeInfo()
        {
            lbl_table.Visible = false;
            lbl_tableno.Visible = false;
            lbl_seats.Visible = false;
            lbl_NumOfSeats.Visible = false;
        }

        private void OrderForm_Load(object sender, EventArgs e)
        {
            loadImages();
            OrderDBDaoImpl dbo = new OrderDBDaoImpl();
        }
        
        /*slide show - forward*/
        private void picForward_Click(object sender, EventArgs e)
        {
            count_image_index += 1;
            if (count_image_index > imageList1.Images.Count - 1)
            {
                count_image_index = 0;
            }
            loadImages();
        }
      
        /*slide show - backward*/
        private void picBack_Click(object sender, EventArgs e)
        {
            count_image_index -= 1;
            if (count_image_index < 0)
            {
                count_image_index = imageList1.Images.Count - 1;
            }
            loadImages();
        }
       
        /*load one picture to the slide show*/
        private void loadImages()
        {
            pic_menu.Image = imageList1.Images[count_image_index];
        }
       
        /*pop out the full menu for the user*/
        private void btnMenuMode_Click(object sender, EventArgs e)
        {
            frmMenuMode f = new frmMenuMode();
            f.ShowDialog();
            DialogResult r = f.DialogResult;
            if (r == DialogResult.OK) {load_ListToView(); }
        }
       

        //common changes for list view have been made here
        private void lstOrders_Click(object sender, EventArgs e)
        {
            try
            {
                int i = lstOrders.SelectedIndices[0];
                lstOrders.Items[i].Checked =
                    (lstOrders.Items[i].Checked == true) ? false : true;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        /*get a remove string - includes the order_num to be removed*/
        private void btnRemove_Click(object sender, EventArgs e)
        {
                lstOrders = orders.RemoveFromListView(lstOrders);
                load_ListToView();
        }

        /*Add as combo*/
        private void btnAdd_Click(object sender, EventArgs e)
        {
            OrderDBDaoImpl dbo = new OrderDBDaoImpl();
            //combo has been set here
            combo = dbo.setOrderCombo(dbo.setCombo());
            try
            {
                dbo.orderAsCombo(combo[count_image_index]);
                tbctrl_orders.SelectedIndex =
                (
                MessageBox.Show("Do you want to see the order cart?", "Hints",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes) ? 1 : 0;
            }
            catch (Exception ex)  {}
            finally {  load_ListToView();}
        }

        /*Confirm the order booking*/
        private void btnConfirm_Click(object sender, EventArgs e)
        {
             if(MessageBox.Show("Do you want to proceed the order?", "Confirmation",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes){
                    Close();
                    MessageBox.Show("Thank you for ordering");
                }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
