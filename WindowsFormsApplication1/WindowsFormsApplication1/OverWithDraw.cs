﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class OverWithDraw : ITransfer
    {
        public double deposite(Account acc, double num)
        {
            acc.Balance += num;
            TransitState(acc);
            return acc.Balance;
        }

        public double withdraw(Account acc, double num)
        {
            double preBalance = acc.Balance;
            acc.Balance = ((acc.Balance - num - 10)>-1000)?acc.Balance - num - 10:acc.Balance;
            if (preBalance == acc.Balance) { MessageBox.Show("Can not with draw that much"); }
            TransitState(acc);
            return acc.Balance;
        }

        public void TransitState(Account acc)
        {
            if (acc.Balance >= 2000)
            {
                acc.AccountContext = new NormalState();
            }
            else if (0 <= acc.Balance && acc.Balance < 2000)
            {
                acc.AccountContext = new WarningState();
            }
            else if (-1000 <= acc.Balance && acc.Balance < 0) {
                acc.AccountContext = new OverWithDraw();
            }
        }

        public override string ToString()
        {
            return "Over Draw";
        }
    }
}
