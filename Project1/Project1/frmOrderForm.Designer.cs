﻿namespace Project1
{
    partial class frmOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOrderForm));
            this.pic_menu = new System.Windows.Forms.PictureBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lbl_tableno = new System.Windows.Forms.Label();
            this.lbl_table = new System.Windows.Forms.Label();
            this.lbl_seats = new System.Windows.Forms.Label();
            this.lbl_NumOfSeats = new System.Windows.Forms.Label();
            this.btnMenuMode = new System.Windows.Forms.Button();
            this.picBack = new System.Windows.Forms.PictureBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lstOrders = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnRemove = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tbctrl_orders = new System.Windows.Forms.TabControl();
            this.tbp_combos = new System.Windows.Forms.TabPage();
            this.picForward = new System.Windows.Forms.PictureBox();
            this.tbp_order = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.pic_menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBack)).BeginInit();
            this.tbctrl_orders.SuspendLayout();
            this.tbp_combos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picForward)).BeginInit();
            this.tbp_order.SuspendLayout();
            this.SuspendLayout();
            // 
            // pic_menu
            // 
            this.pic_menu.Location = new System.Drawing.Point(65, 101);
            this.pic_menu.Margin = new System.Windows.Forms.Padding(4);
            this.pic_menu.Name = "pic_menu";
            this.pic_menu.Size = new System.Drawing.Size(292, 196);
            this.pic_menu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic_menu.TabIndex = 0;
            this.pic_menu.TabStop = false;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(147, 329);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(124, 39);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "&Add Combo";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lbl_tableno
            // 
            this.lbl_tableno.AutoSize = true;
            this.lbl_tableno.Location = new System.Drawing.Point(11, 50);
            this.lbl_tableno.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_tableno.Name = "lbl_tableno";
            this.lbl_tableno.Size = new System.Drawing.Size(70, 16);
            this.lbl_tableno.TabIndex = 13;
            this.lbl_tableno.Text = "&Table NO.";
            // 
            // lbl_table
            // 
            this.lbl_table.AutoSize = true;
            this.lbl_table.Location = new System.Drawing.Point(115, 50);
            this.lbl_table.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_table.Name = "lbl_table";
            this.lbl_table.Size = new System.Drawing.Size(36, 16);
            this.lbl_table.TabIndex = 14;
            this.lbl_table.Text = "&TBD";
            // 
            // lbl_seats
            // 
            this.lbl_seats.AutoSize = true;
            this.lbl_seats.Location = new System.Drawing.Point(278, 50);
            this.lbl_seats.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_seats.Name = "lbl_seats";
            this.lbl_seats.Size = new System.Drawing.Size(72, 16);
            this.lbl_seats.TabIndex = 15;
            this.lbl_seats.Text = "&Own Seats";
            // 
            // lbl_NumOfSeats
            // 
            this.lbl_NumOfSeats.AutoSize = true;
            this.lbl_NumOfSeats.Location = new System.Drawing.Point(400, 50);
            this.lbl_NumOfSeats.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_NumOfSeats.Name = "lbl_NumOfSeats";
            this.lbl_NumOfSeats.Size = new System.Drawing.Size(15, 16);
            this.lbl_NumOfSeats.TabIndex = 16;
            this.lbl_NumOfSeats.Text = "0";
            // 
            // btnMenuMode
            // 
            this.btnMenuMode.Location = new System.Drawing.Point(106, 310);
            this.btnMenuMode.Margin = new System.Windows.Forms.Padding(4);
            this.btnMenuMode.Name = "btnMenuMode";
            this.btnMenuMode.Size = new System.Drawing.Size(216, 39);
            this.btnMenuMode.TabIndex = 17;
            this.btnMenuMode.Text = "&Menu";
            this.btnMenuMode.UseVisualStyleBackColor = true;
            this.btnMenuMode.Click += new System.EventHandler(this.btnMenuMode_Click);
            // 
            // picBack
            // 
            this.picBack.Image = global::Project1.Properties.Resources.image6_1;
            this.picBack.Location = new System.Drawing.Point(6, 125);
            this.picBack.Name = "picBack";
            this.picBack.Size = new System.Drawing.Size(52, 151);
            this.picBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBack.TabIndex = 18;
            this.picBack.TabStop = false;
            this.picBack.Click += new System.EventHandler(this.picBack_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "cb1.jpg");
            this.imageList1.Images.SetKeyName(1, "combo_2.jpg");
            this.imageList1.Images.SetKeyName(2, "combo_3.jpg");
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(329, 310);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(52, 20);
            this.lblTotal.TabIndex = 21;
            this.lblTotal.Text = "&Total :";
            // 
            // lblAmount
            // 
            this.lblAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmount.Location = new System.Drawing.Point(383, 310);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(45, 20);
            this.lblAmount.TabIndex = 22;
            this.lblAmount.Text = "0";
            // 
            // lstOrders
            // 
            this.lstOrders.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lstOrders.Location = new System.Drawing.Point(14, 83);
            this.lstOrders.Name = "lstOrders";
            this.lstOrders.Size = new System.Drawing.Size(401, 207);
            this.lstOrders.TabIndex = 23;
            this.lstOrders.UseCompatibleStateImageBehavior = false;
            this.lstOrders.View = System.Windows.Forms.View.Details;
            this.lstOrders.Click += new System.EventHandler(this.lstOrders_Click);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Order Num";
            this.columnHeader4.Width = 90;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Item Name";
            this.columnHeader1.Width = 130;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Unit Price";
            this.columnHeader2.Width = 110;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Quantity";
            this.columnHeader3.Width = 67;
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(50, 378);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(92, 39);
            this.btnRemove.TabIndex = 25;
            this.btnRemove.Text = "&Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(281, 378);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 39);
            this.button1.TabIndex = 26;
            this.button1.Text = "&Confirm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // tbctrl_orders
            // 
            this.tbctrl_orders.Controls.Add(this.tbp_combos);
            this.tbctrl_orders.Controls.Add(this.tbp_order);
            this.tbctrl_orders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbctrl_orders.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbctrl_orders.Location = new System.Drawing.Point(0, 0);
            this.tbctrl_orders.Name = "tbctrl_orders";
            this.tbctrl_orders.SelectedIndex = 0;
            this.tbctrl_orders.Size = new System.Drawing.Size(659, 469);
            this.tbctrl_orders.TabIndex = 27;
            // 
            // tbp_combos
            // 
            this.tbp_combos.BackgroundImage = global::Project1.Properties.Resources.Restaurant_Chef2;
            this.tbp_combos.Controls.Add(this.picForward);
            this.tbp_combos.Controls.Add(this.btnAdd);
            this.tbp_combos.Controls.Add(this.pic_menu);
            this.tbp_combos.Controls.Add(this.picBack);
            this.tbp_combos.Location = new System.Drawing.Point(4, 27);
            this.tbp_combos.Name = "tbp_combos";
            this.tbp_combos.Padding = new System.Windows.Forms.Padding(3);
            this.tbp_combos.Size = new System.Drawing.Size(651, 438);
            this.tbp_combos.TabIndex = 0;
            this.tbp_combos.Text = "Combos";
            this.tbp_combos.UseVisualStyleBackColor = true;
            // 
            // picForward
            // 
            this.picForward.BackColor = System.Drawing.Color.Transparent;
            this.picForward.Image = global::Project1.Properties.Resources.image5_1;
            this.picForward.Location = new System.Drawing.Point(364, 125);
            this.picForward.Name = "picForward";
            this.picForward.Size = new System.Drawing.Size(48, 151);
            this.picForward.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picForward.TabIndex = 19;
            this.picForward.TabStop = false;
            this.picForward.Click += new System.EventHandler(this.picForward_Click);
            // 
            // tbp_order
            // 
            this.tbp_order.BackgroundImage = global::Project1.Properties.Resources.Restaurant_Chef2;
            this.tbp_order.Controls.Add(this.lstOrders);
            this.tbp_order.Controls.Add(this.btnMenuMode);
            this.tbp_order.Controls.Add(this.lbl_tableno);
            this.tbp_order.Controls.Add(this.button1);
            this.tbp_order.Controls.Add(this.lbl_table);
            this.tbp_order.Controls.Add(this.btnRemove);
            this.tbp_order.Controls.Add(this.lbl_seats);
            this.tbp_order.Controls.Add(this.lbl_NumOfSeats);
            this.tbp_order.Controls.Add(this.lblAmount);
            this.tbp_order.Controls.Add(this.lblTotal);
            this.tbp_order.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbp_order.Location = new System.Drawing.Point(4, 27);
            this.tbp_order.Name = "tbp_order";
            this.tbp_order.Padding = new System.Windows.Forms.Padding(3);
            this.tbp_order.Size = new System.Drawing.Size(651, 438);
            this.tbp_order.TabIndex = 1;
            this.tbp_order.Text = "Order Cart";
            this.tbp_order.UseVisualStyleBackColor = true;
            // 
            // frmOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.BurlyWood;
            this.ClientSize = new System.Drawing.Size(659, 469);
            this.Controls.Add(this.tbctrl_orders);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmOrderForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Order Here";
            this.Load += new System.EventHandler(this.OrderForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pic_menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBack)).EndInit();
            this.tbctrl_orders.ResumeLayout(false);
            this.tbp_combos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picForward)).EndInit();
            this.tbp_order.ResumeLayout(false);
            this.tbp_order.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pic_menu;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lbl_tableno;
        private System.Windows.Forms.Label lbl_table;
        private System.Windows.Forms.Label lbl_seats;
        private System.Windows.Forms.Label lbl_NumOfSeats;
        private System.Windows.Forms.Button btnMenuMode;
        private System.Windows.Forms.PictureBox picBack;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.ListView lstOrders;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl tbctrl_orders;
        private System.Windows.Forms.TabPage tbp_combos;
        private System.Windows.Forms.TabPage tbp_order;
        private System.Windows.Forms.PictureBox picForward;
    }
}