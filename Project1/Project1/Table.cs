﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    class Table
    {
        private int tableNum, numSeats, sharedTable;
        private string selectedDay, timeSlot;
        //private bool sharedTable;

        public int TableNum{get{return tableNum; } set { tableNum = value; } }

        public int NumSeats  {get {return numSeats;} set {numSeats = value; } }

        public string SelectedDay {get {return selectedDay; } set {selectedDay = value; } }

        public string TimeSlot { get { return timeSlot;  } set  { timeSlot = value; } }

        public int SharedTable   {  get  { return sharedTable; }set   {sharedTable = value; } }

        public Table() { }

        public Table(int tableNum, int numSeats, string selectedDay, string timeSlot, int shared)
        {
            TableNum = tableNum;
            NumSeats = numSeats;
            SelectedDay = selectedDay;
            TimeSlot = timeSlot;
            SharedTable = shared;
        }
        
    }
}

