﻿namespace Project1
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbctrl_register = new System.Windows.Forms.TabControl();
            this.tbp_register = new System.Windows.Forms.TabPage();
            this.btn_reset = new System.Windows.Forms.Button();
            this.btn_submit = new System.Windows.Forms.Button();
            this.lbl_optional = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.lbl_email = new System.Windows.Forms.Label();
            this.lbl_password = new System.Windows.Forms.Label();
            this.lbl_user = new System.Windows.Forms.Label();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.txt_user = new System.Windows.Forms.TextBox();
            this.tbp_login = new System.Windows.Forms.TabPage();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtLoginPasswd = new System.Windows.Forms.TextBox();
            this.txtLoginUserName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbp_Passwd = new System.Windows.Forms.TabPage();
            this.txtOldPass = new System.Windows.Forms.TextBox();
            this.lblOldPass = new System.Windows.Forms.Label();
            this.btnCPReset = new System.Windows.Forms.Button();
            this.btnConfim = new System.Windows.Forms.Button();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.txtPasswd = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblConfirmPasswd = new System.Windows.Forms.Label();
            this.lblPasswd = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lbl_lbl = new System.Windows.Forms.Label();
            this.tbctrl_register.SuspendLayout();
            this.tbp_register.SuspendLayout();
            this.tbp_login.SuspendLayout();
            this.tbp_Passwd.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbctrl_register
            // 
            this.tbctrl_register.Controls.Add(this.tbp_register);
            this.tbctrl_register.Controls.Add(this.tbp_login);
            this.tbctrl_register.Controls.Add(this.tbp_Passwd);
            this.tbctrl_register.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbctrl_register.Location = new System.Drawing.Point(12, 12);
            this.tbctrl_register.Name = "tbctrl_register";
            this.tbctrl_register.SelectedIndex = 0;
            this.tbctrl_register.Size = new System.Drawing.Size(266, 376);
            this.tbctrl_register.TabIndex = 0;
            // 
            // tbp_register
            // 
            this.tbp_register.BackColor = System.Drawing.Color.BurlyWood;
            this.tbp_register.Controls.Add(this.btn_reset);
            this.tbp_register.Controls.Add(this.btn_submit);
            this.tbp_register.Controls.Add(this.lbl_optional);
            this.tbp_register.Controls.Add(this.label4);
            this.tbp_register.Controls.Add(this.txt_email);
            this.tbp_register.Controls.Add(this.lbl_email);
            this.tbp_register.Controls.Add(this.lbl_password);
            this.tbp_register.Controls.Add(this.lbl_user);
            this.tbp_register.Controls.Add(this.txt_password);
            this.tbp_register.Controls.Add(this.txt_user);
            this.tbp_register.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbp_register.Location = new System.Drawing.Point(4, 29);
            this.tbp_register.Name = "tbp_register";
            this.tbp_register.Padding = new System.Windows.Forms.Padding(3);
            this.tbp_register.Size = new System.Drawing.Size(258, 343);
            this.tbp_register.TabIndex = 0;
            this.tbp_register.Text = "Register";
            // 
            // btn_reset
            // 
            this.btn_reset.Location = new System.Drawing.Point(156, 235);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(89, 45);
            this.btn_reset.TabIndex = 8;
            this.btn_reset.Text = "&Reset";
            this.btn_reset.UseVisualStyleBackColor = true;
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // btn_submit
            // 
            this.btn_submit.Location = new System.Drawing.Point(27, 235);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(88, 45);
            this.btn_submit.TabIndex = 7;
            this.btn_submit.Text = "&Submit";
            this.btn_submit.UseVisualStyleBackColor = true;
            this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
            // 
            // lbl_optional
            // 
            this.lbl_optional.AutoSize = true;
            this.lbl_optional.Location = new System.Drawing.Point(23, 17);
            this.lbl_optional.Name = "lbl_optional";
            this.lbl_optional.Size = new System.Drawing.Size(66, 16);
            this.lbl_optional.TabIndex = 9;
            this.lbl_optional.Text = "&Optional *";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(74, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "&*";
            // 
            // txt_email
            // 
            this.txt_email.Location = new System.Drawing.Point(145, 158);
            this.txt_email.Name = "txt_email";
            this.txt_email.Size = new System.Drawing.Size(100, 22);
            this.txt_email.TabIndex = 6;
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Location = new System.Drawing.Point(23, 158);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(48, 16);
            this.lbl_email.TabIndex = 4;
            this.lbl_email.Text = "&Email :";
            // 
            // lbl_password
            // 
            this.lbl_password.AutoSize = true;
            this.lbl_password.Location = new System.Drawing.Point(23, 103);
            this.lbl_password.Name = "lbl_password";
            this.lbl_password.Size = new System.Drawing.Size(74, 16);
            this.lbl_password.TabIndex = 2;
            this.lbl_password.Text = "&Password :";
            // 
            // lbl_user
            // 
            this.lbl_user.AutoSize = true;
            this.lbl_user.Location = new System.Drawing.Point(23, 55);
            this.lbl_user.Name = "lbl_user";
            this.lbl_user.Size = new System.Drawing.Size(80, 16);
            this.lbl_user.TabIndex = 0;
            this.lbl_user.Text = "&UserName :";
            // 
            // txt_password
            // 
            this.txt_password.Location = new System.Drawing.Point(145, 103);
            this.txt_password.Name = "txt_password";
            this.txt_password.PasswordChar = '*';
            this.txt_password.Size = new System.Drawing.Size(100, 22);
            this.txt_password.TabIndex = 3;
            // 
            // txt_user
            // 
            this.txt_user.Location = new System.Drawing.Point(145, 52);
            this.txt_user.Name = "txt_user";
            this.txt_user.Size = new System.Drawing.Size(100, 22);
            this.txt_user.TabIndex = 1;
            // 
            // tbp_login
            // 
            this.tbp_login.BackColor = System.Drawing.Color.BurlyWood;
            this.tbp_login.Controls.Add(this.btnReset);
            this.tbp_login.Controls.Add(this.btnLogin);
            this.tbp_login.Controls.Add(this.txtLoginPasswd);
            this.tbp_login.Controls.Add(this.txtLoginUserName);
            this.tbp_login.Controls.Add(this.label7);
            this.tbp_login.Controls.Add(this.label6);
            this.tbp_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbp_login.Location = new System.Drawing.Point(4, 29);
            this.tbp_login.Name = "tbp_login";
            this.tbp_login.Padding = new System.Windows.Forms.Padding(3);
            this.tbp_login.Size = new System.Drawing.Size(258, 343);
            this.tbp_login.TabIndex = 1;
            this.tbp_login.Text = "Login";
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(137, 203);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(100, 48);
            this.btnReset.TabIndex = 5;
            this.btnReset.Text = "&Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(25, 203);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(95, 48);
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "&Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            this.btnLogin.MouseHover += new System.EventHandler(this.btnLogin_MouseHover);
            this.btnLogin.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.btnLogin_PreviewKeyDown);
            // 
            // txtLoginPasswd
            // 
            this.txtLoginPasswd.Location = new System.Drawing.Point(137, 126);
            this.txtLoginPasswd.Name = "txtLoginPasswd";
            this.txtLoginPasswd.PasswordChar = '*';
            this.txtLoginPasswd.Size = new System.Drawing.Size(100, 22);
            this.txtLoginPasswd.TabIndex = 3;
            // 
            // txtLoginUserName
            // 
            this.txtLoginUserName.Location = new System.Drawing.Point(137, 79);
            this.txtLoginUserName.Name = "txtLoginUserName";
            this.txtLoginUserName.Size = new System.Drawing.Size(100, 22);
            this.txtLoginUserName.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 16);
            this.label7.TabIndex = 2;
            this.label7.Text = "&Password :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "&UserName :";
            // 
            // tbp_Passwd
            // 
            this.tbp_Passwd.BackgroundImage = global::Project1.Properties.Resources.Restaurant_Chef2;
            this.tbp_Passwd.Controls.Add(this.txtOldPass);
            this.tbp_Passwd.Controls.Add(this.lblOldPass);
            this.tbp_Passwd.Controls.Add(this.btnCPReset);
            this.tbp_Passwd.Controls.Add(this.btnConfim);
            this.tbp_Passwd.Controls.Add(this.txtConfirmPassword);
            this.tbp_Passwd.Controls.Add(this.txtPasswd);
            this.tbp_Passwd.Controls.Add(this.txtUserName);
            this.tbp_Passwd.Controls.Add(this.lblConfirmPasswd);
            this.tbp_Passwd.Controls.Add(this.lblPasswd);
            this.tbp_Passwd.Controls.Add(this.lblUserName);
            this.tbp_Passwd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbp_Passwd.Location = new System.Drawing.Point(4, 29);
            this.tbp_Passwd.Name = "tbp_Passwd";
            this.tbp_Passwd.Size = new System.Drawing.Size(258, 343);
            this.tbp_Passwd.TabIndex = 2;
            this.tbp_Passwd.Text = "Change Passwd";
            this.tbp_Passwd.UseVisualStyleBackColor = true;
            // 
            // txtOldPass
            // 
            this.txtOldPass.Location = new System.Drawing.Point(123, 82);
            this.txtOldPass.Name = "txtOldPass";
            this.txtOldPass.PasswordChar = '*';
            this.txtOldPass.Size = new System.Drawing.Size(100, 22);
            this.txtOldPass.TabIndex = 3;
            // 
            // lblOldPass
            // 
            this.lblOldPass.AutoSize = true;
            this.lblOldPass.Location = new System.Drawing.Point(23, 85);
            this.lblOldPass.Name = "lblOldPass";
            this.lblOldPass.Size = new System.Drawing.Size(86, 16);
            this.lblOldPass.TabIndex = 2;
            this.lblOldPass.Text = "Old Passwd :";
            // 
            // btnCPReset
            // 
            this.btnCPReset.Location = new System.Drawing.Point(148, 274);
            this.btnCPReset.Name = "btnCPReset";
            this.btnCPReset.Size = new System.Drawing.Size(75, 49);
            this.btnCPReset.TabIndex = 9;
            this.btnCPReset.Text = "&Reset";
            this.btnCPReset.UseVisualStyleBackColor = true;
            this.btnCPReset.Click += new System.EventHandler(this.btnCPReset_Click);
            // 
            // btnConfim
            // 
            this.btnConfim.Location = new System.Drawing.Point(24, 274);
            this.btnConfim.Name = "btnConfim";
            this.btnConfim.Size = new System.Drawing.Size(75, 49);
            this.btnConfim.TabIndex = 8;
            this.btnConfim.Text = "&Confirm";
            this.btnConfim.UseVisualStyleBackColor = true;
            this.btnConfim.Click += new System.EventHandler(this.btnConfim_Click);
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Location = new System.Drawing.Point(123, 197);
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.PasswordChar = '*';
            this.txtConfirmPassword.Size = new System.Drawing.Size(100, 22);
            this.txtConfirmPassword.TabIndex = 7;
            // 
            // txtPasswd
            // 
            this.txtPasswd.Location = new System.Drawing.Point(123, 142);
            this.txtPasswd.Name = "txtPasswd";
            this.txtPasswd.PasswordChar = '*';
            this.txtPasswd.Size = new System.Drawing.Size(100, 22);
            this.txtPasswd.TabIndex = 5;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(123, 31);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(100, 22);
            this.txtUserName.TabIndex = 1;
            // 
            // lblConfirmPasswd
            // 
            this.lblConfirmPasswd.AutoSize = true;
            this.lblConfirmPasswd.Location = new System.Drawing.Point(23, 200);
            this.lblConfirmPasswd.Name = "lblConfirmPasswd";
            this.lblConfirmPasswd.Size = new System.Drawing.Size(62, 16);
            this.lblConfirmPasswd.TabIndex = 6;
            this.lblConfirmPasswd.Text = "Confirm  :";
            // 
            // lblPasswd
            // 
            this.lblPasswd.AutoSize = true;
            this.lblPasswd.Location = new System.Drawing.Point(20, 142);
            this.lblPasswd.Name = "lblPasswd";
            this.lblPasswd.Size = new System.Drawing.Size(74, 16);
            this.lblPasswd.TabIndex = 4;
            this.lblPasswd.Text = "Password :";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(20, 34);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(83, 16);
            this.lblUserName.TabIndex = 0;
            this.lblUserName.Text = "User Name :";
            // 
            // lbl_lbl
            // 
            this.lbl_lbl.BackColor = System.Drawing.Color.White;
            this.lbl_lbl.Location = new System.Drawing.Point(487, 409);
            this.lbl_lbl.Name = "lbl_lbl";
            this.lbl_lbl.Size = new System.Drawing.Size(111, 30);
            this.lbl_lbl.TabIndex = 1;
            // 
            // frmLogin
            // 
            this.AcceptButton = this.btn_submit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Project1.Properties.Resources.image2;
            this.ClientSize = new System.Drawing.Size(598, 439);
            this.Controls.Add(this.lbl_lbl);
            this.Controls.Add(this.tbctrl_register);
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "          Reservations";
            this.tbctrl_register.ResumeLayout(false);
            this.tbp_register.ResumeLayout(false);
            this.tbp_register.PerformLayout();
            this.tbp_login.ResumeLayout(false);
            this.tbp_login.PerformLayout();
            this.tbp_Passwd.ResumeLayout(false);
            this.tbp_Passwd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbctrl_register;
        private System.Windows.Forms.TabPage tbp_register;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.Label lbl_password;
        private System.Windows.Forms.Label lbl_user;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.TextBox txt_user;
        private System.Windows.Forms.TabPage tbp_login;
        private System.Windows.Forms.Label lbl_optional;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.Button btn_submit;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtLoginPasswd;
        private System.Windows.Forms.TextBox txtLoginUserName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl_lbl;
        private System.Windows.Forms.TabPage tbp_Passwd;
        private System.Windows.Forms.Button btnCPReset;
        private System.Windows.Forms.Button btnConfim;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.TextBox txtPasswd;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblConfirmPasswd;
        private System.Windows.Forms.Label lblPasswd;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.TextBox txtOldPass;
        private System.Windows.Forms.Label lblOldPass;
    }
}

