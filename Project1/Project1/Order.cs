﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    public class Order : Menu
    {
        private int orderId, customer_id;

        public int OrderId { get{return orderId;}set{orderId = value;} }
        public int Cid { get { return customer_id; } set { this.customer_id = value; } }
       

        public Order(){}

        //full assigned
        public Order(int orderId, int customer_id, int item_id, string item_name, int unit_price, int quantity)
            : base(item_id,item_name,unit_price, quantity)
        {
            OrderId = orderId;
            Cid = customer_id;
        }

        //Insert an order
        public Order( int customer_id, int item_id, int quantity)
        {
            Cid = customer_id;
            base.Item_id = item_id;
            base.Quantity = quantity;
        }

        //display in order form
        public Order(int orderId, int customer_id, int item_id, int quantity) {
            OrderId = orderId;
            Cid = customer_id;
            base.Item_id = item_id;
            base.Quantity = quantity;
        }

        public override string ToString()
        {
            return  Cid + "" + Item_id +" "+Quantity;
        }
    }
}
