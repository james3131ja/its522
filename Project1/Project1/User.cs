﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    public class User
    {
        private int userId;
        private string  userName, userPassword, userEmail, priority;

        public int UserId { get{return userId;} set  {userId = value; } }

        public string UserName  { get { return userName;} set { userName = value; } }

        public string UserPassword { get { return userPassword;  }set {userPassword = value; } }

        public string UserEmail  { get  { return userEmail;   }set  { userEmail = value; } }

        public string Priority  { get { return priority; }  set { priority = value;  } }

        /*Empty one*/
        public User()
        { }

        /*Only for login checking*/
        public User(string uName, string uPass) {
            UserId = 0;
            UserName = uName;
            UserPassword = uPass;
            UserEmail = "";
            Priority = "";
        }

        /*Add a normal user no priority*/
        public User(string uName, string uPass,string email)
        {
            UserName = uName;
            UserPassword = uPass;
            UserEmail = email;
            Priority = "";
        }

        /*Add an adamin*/
        public User(string uName, string uPass, string email, string priority)
        {
            UserName = uName;
            UserPassword = uPass;
            UserEmail = email;
            Priority = priority;
        }


        /**********************For code testing purpose***********************/

        /*add a test case without database*/
        public User(int cid, string uName, string uPass, string email)
        {
            UserId = cid;
            UserName = uName;
            UserPassword = uPass;
            UserEmail = email;
            Priority = "";
        }

        /*need to check 3 properties here*/
        public override bool Equals(object obj)
        {
            User u = obj as User;
            bool b=false;
            if (userName.Equals(u.UserName) 
                && userPassword.Equals(u.UserPassword)&&Priority.Equals(u.Priority))
            { b = true; }
            return b;
        }

        /*put uname pass and priority to one string*/
        public override string ToString()
        {
            return userName + " " + userPassword + " " + userEmail +" " + Priority;
        }
    }
}
