﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    interface OrderDBDao
    {
        SortedDictionary<int,Menu> loadMenusToList(string sql);
        SortedDictionary<int, Order> loadOrdersToList(string sql);
        void addOrder(Order o);
        void removeOrders(string str);
        string[] setCombo();
        string[] setOrderCombo(string[] combo);
        void orderAsCombo(string str);
        //void addTableOrder(string sql);
    }
}
