﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;

namespace Project1
{
    public interface DatabaseDao
    {
        OracleConnection data_Conn();
        int CountTableItems(string sql);  
    }
}
