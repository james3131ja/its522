﻿namespace MALUO824193130LAB4
{
    partial class CustomerBilling
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblFnameBilling = new System.Windows.Forms.Label();
            this.lblLnameBilling = new System.Windows.Forms.Label();
            this.lblTotalBilling = new System.Windows.Forms.Label();
            this.txtFnameBilling = new System.Windows.Forms.TextBox();
            this.txtLnameBilling = new System.Windows.Forms.TextBox();
            this.txtTotalBilling = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(34, 169);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(147, 169);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblFnameBilling
            // 
            this.lblFnameBilling.AutoSize = true;
            this.lblFnameBilling.Location = new System.Drawing.Point(32, 26);
            this.lblFnameBilling.Name = "lblFnameBilling";
            this.lblFnameBilling.Size = new System.Drawing.Size(57, 13);
            this.lblFnameBilling.TabIndex = 5;
            this.lblFnameBilling.Text = "&First Name";
            // 
            // lblLnameBilling
            // 
            this.lblLnameBilling.AutoSize = true;
            this.lblLnameBilling.Location = new System.Drawing.Point(31, 73);
            this.lblLnameBilling.Name = "lblLnameBilling";
            this.lblLnameBilling.Size = new System.Drawing.Size(58, 13);
            this.lblLnameBilling.TabIndex = 6;
            this.lblLnameBilling.Text = "&Last Name";
            // 
            // lblTotalBilling
            // 
            this.lblTotalBilling.AutoSize = true;
            this.lblTotalBilling.Location = new System.Drawing.Point(32, 119);
            this.lblTotalBilling.Name = "lblTotalBilling";
            this.lblTotalBilling.Size = new System.Drawing.Size(31, 13);
            this.lblTotalBilling.TabIndex = 7;
            this.lblTotalBilling.Text = "&Total";
            // 
            // txtFnameBilling
            // 
            this.txtFnameBilling.Location = new System.Drawing.Point(122, 26);
            this.txtFnameBilling.Name = "txtFnameBilling";
            this.txtFnameBilling.Size = new System.Drawing.Size(100, 20);
            this.txtFnameBilling.TabIndex = 0;
            // 
            // txtLnameBilling
            // 
            this.txtLnameBilling.Location = new System.Drawing.Point(122, 73);
            this.txtLnameBilling.Name = "txtLnameBilling";
            this.txtLnameBilling.Size = new System.Drawing.Size(100, 20);
            this.txtLnameBilling.TabIndex = 1;
            // 
            // txtTotalBilling
            // 
            this.txtTotalBilling.Location = new System.Drawing.Point(122, 119);
            this.txtTotalBilling.Name = "txtTotalBilling";
            this.txtTotalBilling.Size = new System.Drawing.Size(100, 20);
            this.txtTotalBilling.TabIndex = 2;
            // 
            // CustomerBilling
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(253, 218);
            this.Controls.Add(this.txtTotalBilling);
            this.Controls.Add(this.txtLnameBilling);
            this.Controls.Add(this.txtFnameBilling);
            this.Controls.Add(this.lblTotalBilling);
            this.Controls.Add(this.lblLnameBilling);
            this.Controls.Add(this.lblFnameBilling);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Name = "CustomerBilling";
            this.Text = "Customer Billing";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblFnameBilling;
        private System.Windows.Forms.Label lblLnameBilling;
        private System.Windows.Forms.Label lblTotalBilling;
        private System.Windows.Forms.TextBox txtFnameBilling;
        private System.Windows.Forms.TextBox txtLnameBilling;
        private System.Windows.Forms.TextBox txtTotalBilling;
    }
}