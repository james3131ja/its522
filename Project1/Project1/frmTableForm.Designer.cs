﻿namespace Project1
{
    partial class frmTableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbo_time = new System.Windows.Forms.ComboBox();
            this.lbltime = new System.Windows.Forms.Label();
            this.lbl_noofseats = new System.Windows.Forms.Label();
            this.rdo_shared = new System.Windows.Forms.RadioButton();
            this.rdo_nshared = new System.Windows.Forms.RadioButton();
            this.gboshared = new System.Windows.Forms.GroupBox();
            this.cboDays = new System.Windows.Forms.ComboBox();
            this.lblDays = new System.Windows.Forms.Label();
            this.cboTables = new System.Windows.Forms.ComboBox();
            this.lblTables = new System.Windows.Forms.Label();
            this.btnBook = new System.Windows.Forms.Button();
            this.pic_table = new System.Windows.Forms.PictureBox();
            this.updSeats = new System.Windows.Forms.NumericUpDown();
            this.gboshared.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updSeats)).BeginInit();
            this.SuspendLayout();
            // 
            // cbo_time
            // 
            this.cbo_time.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_time.FormattingEnabled = true;
            this.cbo_time.Location = new System.Drawing.Point(202, 286);
            this.cbo_time.Margin = new System.Windows.Forms.Padding(4);
            this.cbo_time.Name = "cbo_time";
            this.cbo_time.Size = new System.Drawing.Size(160, 24);
            this.cbo_time.TabIndex = 1;
            // 
            // lbltime
            // 
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.Location = new System.Drawing.Point(34, 288);
            this.lbltime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(115, 25);
            this.lbltime.TabIndex = 0;
            this.lbltime.Text = "&Time Section :";
            // 
            // lbl_noofseats
            // 
            this.lbl_noofseats.BackColor = System.Drawing.Color.Transparent;
            this.lbl_noofseats.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_noofseats.Location = new System.Drawing.Point(443, 227);
            this.lbl_noofseats.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_noofseats.Name = "lbl_noofseats";
            this.lbl_noofseats.Size = new System.Drawing.Size(112, 25);
            this.lbl_noofseats.TabIndex = 2;
            this.lbl_noofseats.Text = "&Num of Seats :";
            // 
            // rdo_shared
            // 
            this.rdo_shared.AutoSize = true;
            this.rdo_shared.Location = new System.Drawing.Point(29, 37);
            this.rdo_shared.Margin = new System.Windows.Forms.Padding(4);
            this.rdo_shared.Name = "rdo_shared";
            this.rdo_shared.Size = new System.Drawing.Size(70, 20);
            this.rdo_shared.TabIndex = 0;
            this.rdo_shared.TabStop = true;
            this.rdo_shared.Text = "&Shared";
            this.rdo_shared.UseVisualStyleBackColor = true;
            this.rdo_shared.CheckedChanged += new System.EventHandler(this.rdo_shared_CheckedChanged);
            // 
            // rdo_nshared
            // 
            this.rdo_nshared.AutoSize = true;
            this.rdo_nshared.Location = new System.Drawing.Point(167, 37);
            this.rdo_nshared.Margin = new System.Windows.Forms.Padding(4);
            this.rdo_nshared.Name = "rdo_nshared";
            this.rdo_nshared.Size = new System.Drawing.Size(94, 20);
            this.rdo_nshared.TabIndex = 1;
            this.rdo_nshared.TabStop = true;
            this.rdo_nshared.Text = "&Not Shared";
            this.rdo_nshared.UseVisualStyleBackColor = true;
            this.rdo_nshared.CheckedChanged += new System.EventHandler(this.rdo_nshared_CheckedChanged);
            // 
            // gboshared
            // 
            this.gboshared.BackColor = System.Drawing.Color.Transparent;
            this.gboshared.Controls.Add(this.rdo_shared);
            this.gboshared.Controls.Add(this.rdo_nshared);
            this.gboshared.Location = new System.Drawing.Point(446, 288);
            this.gboshared.Margin = new System.Windows.Forms.Padding(4);
            this.gboshared.Name = "gboshared";
            this.gboshared.Padding = new System.Windows.Forms.Padding(4);
            this.gboshared.Size = new System.Drawing.Size(319, 89);
            this.gboshared.TabIndex = 17;
            this.gboshared.TabStop = false;
            this.gboshared.Text = "Shared Option";
            // 
            // cboDays
            // 
            this.cboDays.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDays.FormattingEnabled = true;
            this.cboDays.Location = new System.Drawing.Point(202, 352);
            this.cboDays.Margin = new System.Windows.Forms.Padding(4);
            this.cboDays.Name = "cboDays";
            this.cboDays.Size = new System.Drawing.Size(160, 24);
            this.cboDays.TabIndex = 22;
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.BackColor = System.Drawing.Color.Transparent;
            this.lblDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDays.Location = new System.Drawing.Point(34, 352);
            this.lblDays.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(91, 16);
            this.lblDays.TabIndex = 23;
            this.lblDays.Text = "&Select a Day :";
            // 
            // cboTables
            // 
            this.cboTables.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.cboTables.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTables.FormattingEnabled = true;
            this.cboTables.Location = new System.Drawing.Point(202, 226);
            this.cboTables.Margin = new System.Windows.Forms.Padding(4);
            this.cboTables.Name = "cboTables";
            this.cboTables.Size = new System.Drawing.Size(160, 24);
            this.cboTables.TabIndex = 24;
            // 
            // lblTables
            // 
            this.lblTables.AutoSize = true;
            this.lblTables.BackColor = System.Drawing.Color.Transparent;
            this.lblTables.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTables.Location = new System.Drawing.Point(34, 226);
            this.lblTables.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTables.Name = "lblTables";
            this.lblTables.Size = new System.Drawing.Size(102, 16);
            this.lblTables.TabIndex = 25;
            this.lblTables.Text = "Select a Table :";
            // 
            // btnBook
            // 
            this.btnBook.Location = new System.Drawing.Point(25, 169);
            this.btnBook.Margin = new System.Windows.Forms.Padding(4);
            this.btnBook.Name = "btnBook";
            this.btnBook.Size = new System.Drawing.Size(740, 28);
            this.btnBook.TabIndex = 26;
            this.btnBook.Text = "Book One Table - Click Me";
            this.btnBook.UseVisualStyleBackColor = true;
            this.btnBook.Click += new System.EventHandler(this.btn_table1_Click);
            // 
            // pic_table
            // 
            this.pic_table.BackgroundImage = global::Project1.Properties.Resources.tables;
            this.pic_table.ErrorImage = null;
            this.pic_table.Location = new System.Drawing.Point(1, 1);
            this.pic_table.Name = "pic_table";
            this.pic_table.Size = new System.Drawing.Size(793, 161);
            this.pic_table.TabIndex = 27;
            this.pic_table.TabStop = false;
            // 
            // updSeats
            // 
            this.updSeats.Location = new System.Drawing.Point(645, 227);
            this.updSeats.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.updSeats.Name = "updSeats";
            this.updSeats.Size = new System.Drawing.Size(120, 22);
            this.updSeats.TabIndex = 28;
            // 
            // frmTableForm
            // 
            this.AcceptButton = this.btnBook;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.BurlyWood;
            this.ClientSize = new System.Drawing.Size(794, 462);
            this.Controls.Add(this.updSeats);
            this.Controls.Add(this.pic_table);
            this.Controls.Add(this.btnBook);
            this.Controls.Add(this.lblTables);
            this.Controls.Add(this.cboTables);
            this.Controls.Add(this.lblDays);
            this.Controls.Add(this.cboDays);
            this.Controls.Add(this.gboshared);
            this.Controls.Add(this.lbl_noofseats);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.cbo_time);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmTableForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TableForm";
            this.Load += new System.EventHandler(this.frmTableForm_Load);
            this.gboshared.ResumeLayout(false);
            this.gboshared.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updSeats)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbo_time;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label lbl_noofseats;
        private System.Windows.Forms.RadioButton rdo_shared;
        private System.Windows.Forms.RadioButton rdo_nshared;
        private System.Windows.Forms.GroupBox gboshared;
        private System.Windows.Forms.ComboBox cboDays;
        private System.Windows.Forms.Label lblDays;
        private System.Windows.Forms.ComboBox cboTables;
        private System.Windows.Forms.Label lblTables;
        private System.Windows.Forms.Button btnBook;
        private System.Windows.Forms.PictureBox pic_table;
        private System.Windows.Forms.NumericUpDown updSeats;
    }
}