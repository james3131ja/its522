﻿namespace WindowsFormsApplication1
{
    partial class frmAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAmount = new System.Windows.Forms.Label();
            this.nupAccount = new System.Windows.Forms.NumericUpDown();
            this.cboChoice = new System.Windows.Forms.ComboBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblChoice = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.lblSetState = new System.Windows.Forms.Label();
            this.lsvRecords = new System.Windows.Forms.ListView();
            ((System.ComponentModel.ISupportInitialize)(this.nupAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(480, 111);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(43, 13);
            this.lblAmount.TabIndex = 2;
            this.lblAmount.Text = "&Amount";
            // 
            // nupAccount
            // 
            this.nupAccount.Location = new System.Drawing.Point(634, 109);
            this.nupAccount.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nupAccount.Name = "nupAccount";
            this.nupAccount.Size = new System.Drawing.Size(112, 20);
            this.nupAccount.TabIndex = 3;
            // 
            // cboChoice
            // 
            this.cboChoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboChoice.FormattingEnabled = true;
            this.cboChoice.Location = new System.Drawing.Point(634, 186);
            this.cboChoice.Name = "cboChoice";
            this.cboChoice.Size = new System.Drawing.Size(112, 21);
            this.cboChoice.TabIndex = 5;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(483, 256);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 6;
            this.btnSubmit.Text = "&Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(671, 256);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblChoice
            // 
            this.lblChoice.AutoSize = true;
            this.lblChoice.Location = new System.Drawing.Point(480, 186);
            this.lblChoice.Name = "lblChoice";
            this.lblChoice.Size = new System.Drawing.Size(40, 13);
            this.lblChoice.TabIndex = 4;
            this.lblChoice.Text = "Choice";
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(324, 24);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(111, 13);
            this.lblState.TabIndex = 0;
            this.lblState.Text = "Your Account Status :";
            // 
            // lblSetState
            // 
            this.lblSetState.AutoSize = true;
            this.lblSetState.Location = new System.Drawing.Point(451, 24);
            this.lblSetState.Name = "lblSetState";
            this.lblSetState.Size = new System.Drawing.Size(29, 13);
            this.lblSetState.TabIndex = 1;
            this.lblSetState.Text = "TBD";
            // 
            // lsvRecords
            // 
            this.lsvRecords.Location = new System.Drawing.Point(90, 69);
            this.lsvRecords.Name = "lsvRecords";
            this.lsvRecords.Size = new System.Drawing.Size(293, 210);
            this.lsvRecords.TabIndex = 8;
            this.lsvRecords.UseCompatibleStateImageBehavior = false;
            this.lsvRecords.View = System.Windows.Forms.View.Details;
            // 
            // frmAccount
            // 
            this.AcceptButton = this.btnSubmit;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(817, 312);
            this.Controls.Add(this.lsvRecords);
            this.Controls.Add(this.lblSetState);
            this.Controls.Add(this.lblState);
            this.Controls.Add(this.lblChoice);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.cboChoice);
            this.Controls.Add(this.nupAccount);
            this.Controls.Add(this.lblAmount);
            this.Name = "frmAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Account Details";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nupAccount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.NumericUpDown nupAccount;
        private System.Windows.Forms.ComboBox cboChoice;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblChoice;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Label lblSetState;
        private System.Windows.Forms.ListView lsvRecords;
    }
}

