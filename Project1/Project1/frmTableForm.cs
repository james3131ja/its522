﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Project1
{
    public partial class frmTableForm : Form
    {
        Table tableInfo = new Table();

        public frmTableForm()
        {
            InitializeComponent();
        }

        /*initialze combo box and set radio check*/
        private void frmTableForm_Load(object sender, EventArgs e)
        {
            cbo_time.Items.Add(tableInfo.TimeSlot = "Morning");
            cbo_time.Items.Add(tableInfo.TimeSlot = "Afternoon");
            cbo_time.Items.Add(tableInfo.TimeSlot = "Evening");
            string[] arr = { "MONDAY", "TUESDAY", "WEDSDAY", "THURSDAY", "FRIDAY", "SATDAY", "SUNDAY" };
            cboDays.Items.AddRange(arr);
            string[] arr1 = { "Table1", "Table2", "Table3", "Table4", "Table5", "Table6" };
            cboTables.Items.AddRange(arr1);
            cboTables.SelectedIndex = 0;
            cbo_time.SelectedIndex = 0;
            cboDays.SelectedIndex = 0;
            rdo_shared.Checked = true;
        }

        /*add table order*/
        private void btn_table1_Click(object sender, EventArgs e)
        {
            switch (cboTables.SelectedItem.ToString())
            {
                case "Table1":
                    tableInfo.TableNum = 1;
                    break;
                case "Table2":
                    tableInfo.TableNum = 2;
                    break;
                case "Table3":
                    tableInfo.TableNum = 3;
                    break;
                case "Table4":
                    tableInfo.TableNum = 4;
                    break;
                case "Table5":
                    tableInfo.TableNum = 5;
                    break;
                case "Table6":
                    tableInfo.TableNum = 6;
                    break;
            }
            setBookedDay();
        }

        /*add a table order*/
        public void insert()
        {
            DatabaseConnection conn = new DatabaseConnection();
            string strSQL = "insert into food_tableorder (table_num,user_id,book_day,timeslot,num_of_seat,share_mode) values (" +
                tableInfo.TableNum + "," + SessionImpl.user.UserId + ",'" + tableInfo.SelectedDay +
                "','" + cbo_time.SelectedIndex + "'," + tableInfo.NumSeats + "," +
                tableInfo.SharedTable + ")";
            if (conn.addTableOrder(strSQL) > 0)
            {
                MessageBox.Show("Table Booked");
            }
            else {
                MessageBox.Show("Booking Failed");
            }
        }

        /*set selected day and share mode*/
        public void setBookedDay()
        {
            if (cbo_time.SelectedItem == "" || (rdo_shared.Checked == false && rdo_nshared.Checked == false)||cboDays.SelectedItem==null)
            {
                MessageBox.Show("Please complete all fields.");
            }
            else
            {
                tableInfo.NumSeats = Convert.ToInt32(updSeats.Value);
                tableInfo.SharedTable = (rdo_shared.Checked == true) ? 1 : 0;
                switch (cboDays.SelectedItem.ToString().Substring(0, 3))
                {
                    case "MON": tableInfo.SelectedDay = "MON";
                        break;
                    case "TUE": tableInfo.SelectedDay = "TUE";
                        break;
                    case "WED": tableInfo.SelectedDay = "WED";
                        break;
                    case "THU": tableInfo.SelectedDay = "THU";
                        break;
                    case "FRI": tableInfo.SelectedDay = "FRI";
                        break;
                    case "SAT": tableInfo.SelectedDay = "SAT";
                        break;
                    case "SUN": tableInfo.SelectedDay = "SUN";
                        break;
                }
                insert();
            }
        }

        /*change the table number by radio buttons*/
        private void rdo_nshared_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_nshared.Checked == true)
            { updSeats.Value = 10;
              updSeats.ReadOnly = true;
            }
            if (rdo_shared.Checked == true)
            {updSeats.ReadOnly = false;   }
        }
        
        /*change the table number by radio buttons*/
        private void rdo_shared_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_nshared.Checked == true)
            {
                updSeats.Value = 10;
                updSeats.ReadOnly = true;
            }
            if (rdo_shared.Checked == true)
            { updSeats.ReadOnly = false; }
        }
    }
}
