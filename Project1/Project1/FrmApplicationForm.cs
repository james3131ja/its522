﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1
{
    public partial class FrmApplicationForm : Form
    {
        public FrmApplicationForm()
        {
            InitializeComponent();
        }

        private void manageAccountsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmManageAccount f = new frmManageAccount();
            f.MdiParent = this;
            f.Show();
        }

        private void orderFoodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOrderForm order = new frmOrderForm();
            order.MdiParent = this;
            order.Show();

        }

        private void seatBookingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTableForm table = new frmTableForm();
            table.MdiParent = this;
            table.Show();
        }

        /*load user according to the user priority if user is authorized*/
        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            frmLogin f = new frmLogin();
            f.ShowDialog();
            DialogResult r = f.DialogResult;
     
            if (r == DialogResult.OK && SessionImpl.user.Priority=="")
            {
                lblAccount.Text = SessionImpl.user.UserName;
                showUserMenus();
            }
            else if (r == DialogResult.OK && SessionImpl.user.Priority.Equals("admin"))
            {
                lblAccount.Text = SessionImpl.user.UserName;
                showAdminMenus();
            }
            else {
                lblAccount.Text = "";
            }
           
        }

        private void manageOrdersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmManageOrders f = new frmManageOrders();
            f.MdiParent = this;
            f.Show();
        }

        private void manageTablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmManageTables f = new frmManageTables();
            f.MdiParent = this;
            f.Show();
        }

        private void FrmApplicationForm_Load(object sender, EventArgs e)
        {
            setAllInvisibleMenus();
            lblAccount.Text = "";
        }

        /*
         *Before a user login, not all the menus can be shown
         */
        private void setAllInvisibleMenus() {
            orderFoodToolStripMenuItem.Visible = false;
            seatBookingToolStripMenuItem.Visible = false;
            adminToolStripMenuItem.Visible = false;
            tablesBookedToolStripMenuItem.Visible = false;
        }

        private void showUserMenus() {
            orderFoodToolStripMenuItem.Visible = true;
            seatBookingToolStripMenuItem.Visible = true;
            tablesBookedToolStripMenuItem.Visible = true;
        }

        private void showAdminMenus() {
            orderFoodToolStripMenuItem.Visible = true;
            seatBookingToolStripMenuItem.Visible = true;
            tablesBookedToolStripMenuItem.Visible = true;
            adminToolStripMenuItem.Visible = true;
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setAllInvisibleMenus();
            lblAccount.Text = "";
            SessionImpl.user = null;
            Form[] fArray = this.MdiChildren;
            foreach (Form f in fArray)
            {
                f.Close();
            }
        }

        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
           
        }

        private void tablesBookedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSumTableOrder f = new frmSumTableOrder();
            f.MdiParent = this;
            f.Show();
        }

    }
}
