﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MALUO824193130LAB4
{
    public partial class CustomerService : Form
    {
      
        public CustomerService()
        {
            InitializeComponent();
        }

        private void customerInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomerInfo f2 = new CustomerInfo();
            f2.MdiParent = this;
            f2.Show();
        }

        private void customerOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomerOrder f3 = new CustomerOrder();
            f3.MdiParent = this;
            f3.Show();
        }

        private void customerBillingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomerBilling f4 = new CustomerBilling();
            f4.MdiParent = this;
            f4.Show();
        }

        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }
    }
}
