﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1
{
    public class MyValidate
    {
        public static bool checkConfirmPass(TextBox name,TextBox pass,TextBox confirm,TextBox oldpass) {
            UserDaoImpl dbo = new UserDaoImpl();
            User u = new User(name.Text,pass.Text);
            return (name.Text!=""&&pass.Text!=""&&confirm.Text!=""&&pass.Text.Equals(confirm.Text)
                &&dbo.getUserPasswd(u).Equals(oldpass.Text))?true:false;
        }

        public static bool verifiedEmail(TextBox textbox) {

            bool isEmail = false;
            if (textbox.Text != "")
            {
                isEmail=
                Regex.IsMatch(textbox.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");
                if (!isEmail) { MessageBox.Show("Please input a valid email address"); }
            }
            return isEmail;
        }

        public static bool checkRegisterForm(TextBox username, TextBox Password) {
            bool b=true;
            if (username.Text == "" || Password.Text == "")
            {
                MessageBox.Show("Complete all the Mandatory fields!!!");
                b = false;
            }
            return b;
        }

      
    }
}
