﻿namespace Project1
{
    partial class frmManageOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch = new System.Windows.Forms.Button();
            this.cboChoice = new System.Windows.Forms.ComboBox();
            this.txtTarget = new System.Windows.Forms.TextBox();
            this.lblMeans = new System.Windows.Forms.Label();
            this.lblTarget = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRemove = new System.Windows.Forms.Button();
            this.dgv_ManageOrders = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ManageOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(298, 24);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 57);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "&Perform Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cboChoice
            // 
            this.cboChoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboChoice.FormattingEnabled = true;
            this.cboChoice.Items.AddRange(new object[] {
            "User Name",
            "User Id",
            "All"});
            this.cboChoice.Location = new System.Drawing.Point(145, 24);
            this.cboChoice.Name = "cboChoice";
            this.cboChoice.Size = new System.Drawing.Size(108, 21);
            this.cboChoice.TabIndex = 21;
            // 
            // txtTarget
            // 
            this.txtTarget.Location = new System.Drawing.Point(145, 61);
            this.txtTarget.Name = "txtTarget";
            this.txtTarget.Size = new System.Drawing.Size(108, 20);
            this.txtTarget.TabIndex = 22;
            // 
            // lblMeans
            // 
            this.lblMeans.AutoSize = true;
            this.lblMeans.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMeans.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblMeans.Location = new System.Drawing.Point(53, 24);
            this.lblMeans.Name = "lblMeans";
            this.lblMeans.Size = new System.Drawing.Size(76, 16);
            this.lblMeans.TabIndex = 23;
            this.lblMeans.Text = "&Search By :";
            // 
            // lblTarget
            // 
            this.lblTarget.AutoSize = true;
            this.lblTarget.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTarget.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTarget.Location = new System.Drawing.Point(53, 64);
            this.lblTarget.Name = "lblTarget";
            this.lblTarget.Size = new System.Drawing.Size(54, 16);
            this.lblTarget.TabIndex = 24;
            this.lblTarget.Text = "&Target :";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.lblTarget);
            this.panel1.Controls.Add(this.lblMeans);
            this.panel1.Controls.Add(this.cboChoice);
            this.panel1.Controls.Add(this.txtTarget);
            this.panel1.Location = new System.Drawing.Point(143, 317);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(420, 102);
            this.panel1.TabIndex = 25;
            // 
            // btnRemove
            // 
            this.btnRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.Location = new System.Drawing.Point(169, 261);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(375, 35);
            this.btnRemove.TabIndex = 26;
            this.btnRemove.Text = "&Remove Orders";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // dgv_ManageOrders
            // 
            this.dgv_ManageOrders.BackgroundColor = System.Drawing.Color.MistyRose;
            this.dgv_ManageOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ManageOrders.Location = new System.Drawing.Point(162, 32);
            this.dgv_ManageOrders.Name = "dgv_ManageOrders";
            this.dgv_ManageOrders.Size = new System.Drawing.Size(401, 183);
            this.dgv_ManageOrders.TabIndex = 27;
            // 
            // frmManageOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImage = global::Project1.Properties.Resources.image9_12;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(710, 449);
            this.Controls.Add(this.dgv_ManageOrders);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmManageOrders";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manage Orders";
            this.Load += new System.EventHandler(this.frmManageOrders_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ManageOrders)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ComboBox cboChoice;
        private System.Windows.Forms.TextBox txtTarget;
        private System.Windows.Forms.Label lblMeans;
        private System.Windows.Forms.Label lblTarget;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.DataGridView dgv_ManageOrders;
    }
}