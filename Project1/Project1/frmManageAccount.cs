﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Project1
{
    public partial class frmManageAccount : Form
    {

        DatabaseConnection conn = null;
        
        public frmManageAccount()
        {
            InitializeComponent();
            conn = new DatabaseConnection();
            cbo_priority.SelectedIndex = 1;
        }

        private void btn_AddAccount_Click(object sender, EventArgs e)
        {
            try
            {
                if (MyValidate.verifiedEmail(txt_EmailId) || txt_EmailId.Text == "")
                {
                    string strSQL = "Insert into food_users(uname,passwd,email,priority) values ('" + txt_UserName.Text + "','" +
                     txt_Password.Text + "','" + txt_EmailId.Text + "','" + cbo_priority.SelectedItem.ToString().ToLower() + "')";
                    int i = conn.loadDataGridView(dgv_accounts, strSQL);
                    if (i == 1) { MessageBox.Show("Account Added Successfully!!!"); }
                    else
                    {
                        MessageBox.Show("User Name Already Exists!!!");
                    }
                }
                loadAccounts();
            }
            catch (OracleException ex){}
            catch (Exception ex){ }
            cleanText();
        }

        private void btn_RemoveAccount_Click(object sender, EventArgs e)
        {
            try
            {
                string strDelete = "Delete from food_users where uname ='" + dgv_accounts.CurrentRow.Cells[1].Value.ToString() + "'";
                int i = conn.loadDataGridView(dgv_accounts, strDelete);
                if (i > 0) { MessageBox.Show("Account Deleted...!!!!"); }
                loadAccounts();
            }
            catch {  }
        }

        private void frmManageAccount_Load(object sender, EventArgs e)
        {
            loadAccounts();
        }

        private void btn_UpdateAccount_Click(object sender, EventArgs e)
        {

            try
            {

                if (txt_NewPassword.Text == txt_Password.Text && (txt_NewPassword.Text != "" && txt_Password.Text != ""))
                {

                    if (MyValidate.verifiedEmail(txt_EmailId) || txt_EmailId.Text == "")
                    {
                        string strUpdate = "Update food_users set passwd ='" + txt_Password.Text + "', email = '" + txt_EmailId.Text + "', priority = '" + cbo_priority.Text + "' where uname='" + dgv_accounts.SelectedRows[0].Cells[1].Value + "'";
                        int i = conn.loadDataGridView(dgv_accounts, strUpdate);
                        if (i > 0) { MessageBox.Show("Account Updated Successfully...!!!!"); }
                        else { MessageBox.Show("Update failed"); }
                    }
                    loadAccounts();
                }

                else if (txt_NewPassword.Text == "" && txt_Password.Text=="")
                {

                    if (MyValidate.verifiedEmail(txt_EmailId) || txt_EmailId.Text == "")
                    {
                        string strUpdate = "Update food_users set email = '" + txt_EmailId.Text + "', priority = '" + cbo_priority.Text + "' where uname='" + dgv_accounts.SelectedRows[0].Cells[1].Value + "'";
                        //MessageBox.Show("dasfadsfasdfadf");
                        int i = conn.loadDataGridView(dgv_accounts, strUpdate);
                        if (i > 0) { MessageBox.Show("Account Updated Successfully...!!!!"); }
                        else { MessageBox.Show("Update failed"); }
                    }

                    loadAccounts();
                }
                else
                {
                    MessageBox.Show("Passwords should be same");
                }
            }
            catch { 
            
            }

            cleanText();
        }

        private void loadAccounts() {
            conn.loadDataGridView(dgv_accounts, "select * from food_users");
        }

        private void cleanText() {
            txt_UserName.Text = "";
            txt_Password.Text = "";
            txt_EmailId.Text = "";
            txt_NewPassword.Text = "";
            cbo_priority.SelectedItem = "";
        }

        /*laod user name and user mode to the form*/
        private void dgv_accounts_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                txt_UserName.Text = dgv_accounts.SelectedRows[0].Cells[1].Value.ToString();
                cbo_priority.SelectedIndex = (dgv_accounts.SelectedRows[0].Cells[4].Value.ToString() == "admin") ? 0 : 1;
            }
            catch { 
            
            }
        }
    }
}
