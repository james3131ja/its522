﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1
{
    public partial class frmManageOrders : Form
    {
        DatabaseConnection conn = null;

        
        public frmManageOrders()
        {
            InitializeComponent();
            conn = new DatabaseConnection();
            cboChoice.SelectedIndex = 2;
        }

        
        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                string strDelete = "Delete from food_orders where order_id=" + dgv_ManageOrders.SelectedRows[0].Cells[1].Value.ToString();
                int i = conn.loadDataGridView(dgv_ManageOrders, strDelete);
                if (i == 1) { MessageBox.Show("Order Removed"); }
                loadOrders();
                resetText();
            }
            catch (Exception ex)
            { }
        }

        
        private void btnSearch_Click(object sender, EventArgs e)
        {
            switch (cboChoice.SelectedIndex)
            {
                case 1:
                    string strSearch = getOrderString()+" where user_id = '" + txtTarget.Text + "'";
                    conn.loadDataGridView(dgv_ManageOrders, strSearch);
                    txtTarget.Text = "";
                    break;

                case 0:
                    string strSea = getOrderString()+" where uname = '" + txtTarget.Text + "'";
                    txtTarget.Text = "";
                    conn.loadDataGridView(dgv_ManageOrders, strSea);
                    break;

                case 2:
                    loadOrders();
                    txtTarget.Text = "";
                    break;
            }
            resetText();
        }



        private void frmManageOrders_Load(object sender, EventArgs e)
        {
            loadOrders();
        }
        

        private void loadOrders(){
            conn.loadDataGridView(dgv_ManageOrders, getOrderString());
        }


        private void resetText() {
            txtTarget.Text = "";
        }


        /*set and return a common part of sql string since it is a little long*/
        private string getOrderString() {
            return "select user_id,food_orders.order_id,uname,item_id,quantity from food_orders join food_users using(user_id) join food_menu using(item_id)";
        }
    }
}
