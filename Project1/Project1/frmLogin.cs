﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Project1
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        /*Create and Submit the object*/
        private void btn_submit_Click(object sender, EventArgs e)
        {

            if (
                (MyValidate.verifiedEmail(txt_email) || txt_email.Text=="")&&
                MyValidate.checkRegisterForm(txt_user,txt_password)
                )
            {
                User u = new User(txt_user.Text, txt_password.Text, txt_email.Text);
                //MessageBox.Show(u.ToString());
                UserDaoImpl dbo = new UserDaoImpl();
                if (dbo.addUser(u) > 0) { MessageBox.Show("Register Successfully"); }
                else { MessageBox.Show("User name already existed"); }
            }
            resetSubmitForm();
        }

        /*when the mouse is over the button, 
         * check the login and priority state arrcording to the user name and password input
         *and set the dialog result of the button
         */
        private void btnLogin_MouseHover(object sender, EventArgs e)
        {
            checkUser();
        }

        private void btnLogin_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            checkUser();
        }

        private void checkUser() {
            if (MyValidate.checkRegisterForm(txtLoginUserName, txtLoginPasswd))
            {
                User u = new User(txtLoginUserName.Text, txtLoginPasswd.Text);
                DatabaseDaoImpl dbo = new DatabaseDaoImpl();
                UserDaoImpl udo = new UserDaoImpl();
                int login = 0;
                int priority = 0;
                try
                {
                    login = dbo.CountTableItems("select count(*) from food_users where uname='" + u.UserName + "' and passwd='" + u.UserPassword + "'");
                    priority = dbo.CountTableItems("select count(*) from food_users where uname='" + u.UserName + "' and priority='admin'");
                    u.Priority = (priority > 0) ? "admin" : "";
                    u.UserId = udo.getUserID(u);
                    SessionImpl.user = u;
                }
                catch (OracleException ex)
                {
                }
                if (login > 0)
                {
                    this.btnLogin.DialogResult = DialogResult.OK;
                }
            }
        }

        /*if user authorized, login and show a welcome message 
         *or a error message will be presented and reset the form
         */
        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (btnLogin.DialogResult == DialogResult.OK)
            {
                //Close();
                MessageBox.Show("Welcome " + SessionImpl.user.UserName, "Welcome",
                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            else {
                resetSubmitForm();
                MessageBox.Show("Username or password doesn't match!!!", "Warning",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.btnLogin.DialogResult = DialogResult.Cancel;
            }
        }

      

        private void btnConfim_Click(object sender, EventArgs e)
        {
            if (MyValidate.checkConfirmPass(txtUserName, txtPasswd, txtConfirmPassword,txtOldPass))
            {
                UserDaoImpl dbo = new UserDaoImpl();
                User u = new User(txtUserName.Text, txtPasswd.Text);
                if (dbo.changePassword(u) > 0) {
                    MessageBox.Show("User Updated");
                }
            }
            else {
                MessageBox.Show("Password doesnt match or blank fields exist");
            }
        }


        /*******************reset text*******************/
        private void btn_reset_Click(object sender, EventArgs e)
        {
            resetSubmitForm();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            resetSubmitForm();
        }

        private void resetSubmitForm()
        {
            txt_user.Text = "";
            txt_password.Text = "";
            txt_email.Text = "";
            txtLoginPasswd.Text = "";
            txtLoginUserName.Text = "";
            txtUserName.Text = "";
            txtPasswd.Text = "";
            txtConfirmPassword.Text = "";
            txtOldPass.Text = "";
        }

        private void btnCPReset_Click(object sender, EventArgs e)
        {
            resetSubmitForm();
        }


       
    }
}
