﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1
{
    class OrdersForm
    {
        private SortedDictionary<int, Order> orderList = null;

        public SortedDictionary<int, Order> OrderList { get { return orderList; } set { orderList = value; } }

        public OrdersForm() { orderList = new SortedDictionary<int,Order>(); }

        public double CalculateOrderTotal()
        {
            double orderTotal = 0;
            double credit = 0;
            foreach (Order order in orderList.Values)
            {
                orderTotal += (Convert.ToInt32(order.UnitPrice) 
                    * Convert.ToDouble(order.Quantity));
                credit = ((int)orderTotal / 100)*50;
                orderTotal = (orderTotal - credit)*(1+0.13);
            }
            return orderTotal;
        }

        public ListView loadListToView(ListView lst) {
           
            OrderDBDaoImpl dbo = new OrderDBDaoImpl();
            //only load the user in session here.
            string sql = "select order_id,product_name,unit_price,quantity FROM food_orders join food_menu using (item_id) where user_id=" + SessionImpl.user.UserId;
            orderList = dbo.loadOrdersToList(sql);
            foreach (Order o in orderList.Values)
            {
                ListViewItem i = new ListViewItem(o.OrderId.ToString());
                i.SubItems.Add(o.Name);
                i.SubItems.Add("$ " + o.UnitPrice.ToString());
                i.SubItems.Add(o.Quantity.ToString());
                lst.Items.Add(i);
            } 
            return lst;
        }

        public ListView RemoveFromListView(ListView lst) {

            OrderDBDaoImpl dbo = new OrderDBDaoImpl();
            String removeFromList = "";
            int count = lst.CheckedItems.Count;
            int index = 0;
            foreach (ListViewItem i in lst.CheckedItems)
            {
                removeFromList += (index == count - 1) ? i.SubItems[0].Text : i.SubItems[0].Text + ";";
                i.Remove();
                index++;
            }
            //MessageBox.Show();
            dbo.removeOrders(removeFromList);
            return lst;
        }
    }
}
