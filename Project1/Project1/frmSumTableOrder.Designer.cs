﻿namespace Project1
{
    partial class frmSumTableOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblUname = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.dgvTableOrders = new System.Windows.Forms.DataGridView();
            this.lblUid = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblnote = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTableOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUname
            // 
            this.lblUname.AutoSize = true;
            this.lblUname.BackColor = System.Drawing.Color.Transparent;
            this.lblUname.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblUname.Location = new System.Drawing.Point(622, 113);
            this.lblUname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUname.Name = "lblUname";
            this.lblUname.Size = new System.Drawing.Size(83, 16);
            this.lblUname.TabIndex = 0;
            this.lblUname.Text = "User Name :";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblName.Location = new System.Drawing.Point(776, 113);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(36, 16);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "TBD";
            // 
            // dgvTableOrders
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTableOrders.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvTableOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTableOrders.Location = new System.Drawing.Point(94, 249);
            this.dgvTableOrders.Margin = new System.Windows.Forms.Padding(4);
            this.dgvTableOrders.Name = "dgvTableOrders";
            this.dgvTableOrders.Size = new System.Drawing.Size(732, 187);
            this.dgvTableOrders.TabIndex = 2;
            // 
            // lblUid
            // 
            this.lblUid.AutoSize = true;
            this.lblUid.BackColor = System.Drawing.Color.Transparent;
            this.lblUid.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblUid.Location = new System.Drawing.Point(113, 113);
            this.lblUid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUid.Name = "lblUid";
            this.lblUid.Size = new System.Drawing.Size(57, 16);
            this.lblUid.TabIndex = 3;
            this.lblUid.Text = "User Id :";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.BackColor = System.Drawing.Color.Transparent;
            this.lblID.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblID.Location = new System.Drawing.Point(218, 113);
            this.lblID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(36, 16);
            this.lblID.TabIndex = 4;
            this.lblID.Text = "TBD";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(418, 444);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 28);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblnote
            // 
            this.lblnote.AutoSize = true;
            this.lblnote.BackColor = System.Drawing.Color.Transparent;
            this.lblnote.ForeColor = System.Drawing.Color.White;
            this.lblnote.Location = new System.Drawing.Point(328, 29);
            this.lblnote.Name = "lblnote";
            this.lblnote.Size = new System.Drawing.Size(269, 16);
            this.lblnote.TabIndex = 6;
            this.lblnote.Text = "*Contact Admin for cancellation of a booking";
            // 
            // frmSumTableOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Project1.Properties.Resources.background;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(923, 498);
            this.Controls.Add(this.lblnote);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.lblUid);
            this.Controls.Add(this.dgvTableOrders);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblUname);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmSumTableOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Summary of Tables";
            this.Load += new System.EventHandler(this.frmSumTableOrder_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTableOrders)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUname;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.DataGridView dgvTableOrders;
        private System.Windows.Forms.Label lblUid;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblnote;
    }
}