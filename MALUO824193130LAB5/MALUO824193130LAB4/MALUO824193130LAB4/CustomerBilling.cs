﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MALUO824193130LAB4
{
    public partial class CustomerBilling : Form
    {
        CustomerService f1 = null;
        public CustomerBilling()
        {
            InitializeComponent();
        }
        public CustomerBilling(Order o,CustomerService f1)
        {
            InitializeComponent();
            this.txtFnameBilling.Text = o.Fname;
            this.txtLnameBilling.Text = o.Lname;
            this.txtTotalBilling.Text = o.Total.ToString();
            this.f1 = f1;
        }
        public CustomerBilling(Order o)
        {
            InitializeComponent();

            this.txtFnameBilling.Text = o.Fname;
            this.txtLnameBilling.Text = o.Lname;
            this.txtTotalBilling.Text = o.Total.ToString();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Welcome come "+this.txtFnameBilling.Text.ToString()+"  "+this.txtLnameBilling.Text.ToString()+"\n"+
            "The total price is: "+txtTotalBilling.Text.ToString());
        }
    }
}
