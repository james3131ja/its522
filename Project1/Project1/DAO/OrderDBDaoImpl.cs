﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using System.Windows.Forms;

namespace Project1
{
    public class OrderDBDaoImpl : OrderDBDao
    {

        public SortedDictionary<int, Menu> loadMenusToList(string sql)
        {
            DatabaseDaoImpl dbo = new DatabaseDaoImpl();
            OracleConnection conn = dbo.data_Conn();
            SortedDictionary<int, Menu> menuList = null;
            try
            {
                
                OracleCommand cmd = new OracleCommand(sql,conn);
                conn.Open();
                OracleDataReader reader = cmd.ExecuteReader();
                menuList = new SortedDictionary<int, Menu>();
                while (reader.Read())
                {
                    Menu m = new Menu();
                    m.Item_id = (reader.IsDBNull(0)) ? 0 : reader.GetInt32(0);
                    m.Name = (reader.IsDBNull(1)) ? "" : reader.GetString(1);
                    m.UnitPrice = (reader.IsDBNull(2)) ? 0.0 : Convert.ToDouble( reader.GetString(2));
                    m.Desc = (reader.IsDBNull(3)) ? "" : reader.GetString(3);
                    m.Category = (reader.IsDBNull(4)) ? "" : reader.GetString(4);
                    m.ComboNum = (reader.IsDBNull(5)) ? 0 : reader.GetInt32(5);
                    //MessageBox.Show("adding");  
                    menuList.Add(menuList.Count, m);
                }
            }
            catch (OracleException ex) { MessageBox.Show(ex.Message); }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return menuList;
        }

        public SortedDictionary<int, Order> loadOrdersToList(string sql) {
            DatabaseDaoImpl dbo = new DatabaseDaoImpl();
            OracleConnection conn = dbo.data_Conn();
            SortedDictionary<int, Order> orderList = null;
            try
            {
                OracleCommand cmd = new OracleCommand(sql, conn);
                conn.Open();
                OracleDataReader reader = cmd.ExecuteReader();
                orderList = new SortedDictionary<int, Order>();
                while (reader.Read())
                {
                    Order o = new Order();
                    o.OrderId = (reader.IsDBNull(0)) ? 0 : reader.GetInt32(0);
                    o.Name = (reader.IsDBNull(1)) ? "" : reader.GetString(1);
                    o.UnitPrice = (reader.IsDBNull(2)) ? 0 : Convert.ToDouble(reader.GetString(2));
                    o.Quantity = (reader.IsDBNull(3)) ? 0 : reader.GetInt32(3);
                    orderList.Add(orderList.Count, o);
                }
            }
            catch (OracleException ex) { MessageBox.Show(ex.Message); }
            catch (Exception ex){MessageBox.Show(ex.Message);}
            finally { conn.Close();}
            return orderList;
        }

        public void addOrder(Order o) {
            string sql = "insert into food_orders (user_id,item_id,quantity) values(" + o.Cid + "," + o.Item_id + "," + o.Quantity + ")";
            OracleConnection conn = null;
            try
            {
                DatabaseDaoImpl dbo = new DatabaseDaoImpl();
                conn = dbo.data_Conn();
                OracleCommand cmd = new OracleCommand(sql, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                //MessageBox.Show("Order "+o.ToString() +"added");
            }
            catch (OracleException ex)
            {
                MessageBox.Show("addOrder " + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally {
                conn.Close();
            }
        }

        public void removeOrders(string removeFromList) {
            OracleConnection conn = null;
            string sql = "";
            string[] strarray = null;
            try
            {
                DatabaseDaoImpl dbo = new DatabaseDaoImpl();
                conn = dbo.data_Conn();
                strarray = removeFromList.Split(new char[] {';'});
                conn.Open();
                foreach (string i in strarray)
                {
                    sql = "delete from food_orders where order_id="+i;
                    OracleCommand cmd = new OracleCommand(sql, conn);
                    cmd.ExecuteNonQuery();
               
                }
                MessageBox.Show("Orders Removed!!!");
            }
            catch (OracleException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        //get combo index - combo nums array
        public string[] setCombo() {
            DatabaseDaoImpl dbo = new DatabaseDaoImpl();       
            string sql = "select count( distinct combonum) from food_menu";
            OracleConnection conn = null;
            int count = dbo.CountTableItems(sql);
            int index = 0;
            string[] strarray = new string[count];
            sql = "select distinct combonum from food_menu";
            try
            {
                conn = dbo.data_Conn();
                conn.Open();
                OracleCommand cmd = new OracleCommand(sql, conn);
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read()) { 
                    strarray[index] = (reader.IsDBNull(0))?"":reader.GetInt32(0).ToString();
                    if(!reader.IsDBNull(0)){
                        index++;
                    }
                }
            }
            catch (OracleException ex)
            {
                MessageBox.Show("set combo "+ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("set combo "+ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return strarray;
        }

        //set order combo - value of the combo array is the combo string which contains info inside a combo
        public string[] setOrderCombo(string[] combo) {
            DatabaseDaoImpl dbo = new DatabaseDaoImpl();
            OracleConnection conn = null;
            int index = 0;
            string sql="";
            try
            {
                conn = dbo.data_Conn();
                conn.Open();
                foreach (string i in combo)
                {
                    sql = "select item_id from food_menu where combonum="+Convert.ToInt32(i);
                    OracleCommand cmd = new OracleCommand(sql, conn);
                    OracleDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        combo[index] += (reader.IsDBNull(0)) ? "" : reader.GetInt32(0)+";";
                    }
                    combo[index] = combo[index].Substring(1, combo[index].Length - 2);
                    index++;
                }
            }
            catch (OracleException ex)
            {
                MessageBox.Show("set order combo "+ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("set ordercombo "+ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return combo;
        }


        public void orderAsCombo(string combo) {
            DatabaseDaoImpl dbo = new DatabaseDaoImpl();
            OracleConnection conn = null;
            string sql = "";
            int index = 0;
            sql = "select count(*) from food_orders";
            int count = dbo.CountTableItems(sql);
            try
            {
                conn = dbo.data_Conn();
                conn.Open();
                foreach (string i in combo.Split(new char[]{';'}))
                {
                    sql = "insert into food_orders values("+count+","+SessionImpl.user.UserId+","+Convert.ToInt32(i)+",1)";
                    OracleCommand cmd = new OracleCommand(sql, conn);
                    cmd.ExecuteNonQuery();
                    count++;
                    index++;
                }
            }
            catch (OracleException ex)
            {
                MessageBox.Show("order combo "+ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("order  combo "+ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        public int getTableNumByUserID(User u) { 
            int num=0;
            DatabaseDaoImpl dbo = new DatabaseDaoImpl();
            OracleConnection conn = null;
            string sql = "";
            sql = "select table_num from food_tableorder join food_users using(user_id) where user_id ='"+SessionImpl.user.UserId+"'";
            try
            {
                conn = dbo.data_Conn();
                conn.Open();
                OracleCommand cmd = new OracleCommand(sql, conn);
                OracleDataReader reader = cmd.ExecuteReader();
                reader.Read();
                num = Convert.ToInt32(reader["table_num"]);
            }
            catch (OracleException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();

            }
            return num;
        }
        
       
        
    }
}
