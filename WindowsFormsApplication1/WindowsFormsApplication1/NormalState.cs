﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class NormalState : ITransfer
    {
        
        public double deposite(Account acc, double num) {
            acc.Balance += num;  
            return acc.Balance;
        }

        //already verified the money left
        public double withdraw(Account acc, double num) {
            acc.Balance = acc.Balance - num;
            TransitState(acc);
            return acc.Balance;
        }

        public void TransitState(Account acc)
        {
            if (acc.Balance >= 2000) {
                acc.AccountContext = new NormalState();
            }else if(0<=acc.Balance && acc.Balance<2000){
                acc.AccountContext = new WarningState(); 
            }
            else if (-1000 <= acc.Balance && acc.Balance < 0) {
                acc.AccountContext = new OverWithDraw();
            }
        }

        public override string ToString()
        {
            return "Normal";
        }
    }
}
