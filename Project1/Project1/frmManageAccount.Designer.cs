﻿namespace Project1
{
    partial class frmManageAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_AddAccount = new System.Windows.Forms.Button();
            this.btn_RemoveAccount = new System.Windows.Forms.Button();
            this.btn_UpdateAccount = new System.Windows.Forms.Button();
            this.lbl_UserName = new System.Windows.Forms.Label();
            this.lbl_Password = new System.Windows.Forms.Label();
            this.lbl_ConfirmPassword = new System.Windows.Forms.Label();
            this.lbl_Email = new System.Windows.Forms.Label();
            this.txt_UserName = new System.Windows.Forms.TextBox();
            this.txt_Password = new System.Windows.Forms.TextBox();
            this.txt_NewPassword = new System.Windows.Forms.TextBox();
            this.txt_EmailId = new System.Windows.Forms.TextBox();
            this.lblPriority = new System.Windows.Forms.Label();
            this.cbo_priority = new System.Windows.Forms.ComboBox();
            this.dgv_accounts = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_accounts)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_AddAccount
            // 
            this.btn_AddAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddAccount.Location = new System.Drawing.Point(753, 103);
            this.btn_AddAccount.Name = "btn_AddAccount";
            this.btn_AddAccount.Size = new System.Drawing.Size(96, 90);
            this.btn_AddAccount.TabIndex = 3;
            this.btn_AddAccount.Text = "&Add Account";
            this.btn_AddAccount.UseVisualStyleBackColor = true;
            this.btn_AddAccount.Click += new System.EventHandler(this.btn_AddAccount_Click);
            // 
            // btn_RemoveAccount
            // 
            this.btn_RemoveAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RemoveAccount.Location = new System.Drawing.Point(757, 353);
            this.btn_RemoveAccount.Name = "btn_RemoveAccount";
            this.btn_RemoveAccount.Size = new System.Drawing.Size(92, 90);
            this.btn_RemoveAccount.TabIndex = 4;
            this.btn_RemoveAccount.Text = "&Remove Account";
            this.btn_RemoveAccount.UseVisualStyleBackColor = true;
            this.btn_RemoveAccount.Click += new System.EventHandler(this.btn_RemoveAccount_Click);
            // 
            // btn_UpdateAccount
            // 
            this.btn_UpdateAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_UpdateAccount.Location = new System.Drawing.Point(757, 225);
            this.btn_UpdateAccount.Name = "btn_UpdateAccount";
            this.btn_UpdateAccount.Size = new System.Drawing.Size(92, 90);
            this.btn_UpdateAccount.TabIndex = 5;
            this.btn_UpdateAccount.Text = "&Update Account";
            this.btn_UpdateAccount.UseVisualStyleBackColor = true;
            this.btn_UpdateAccount.Click += new System.EventHandler(this.btn_UpdateAccount_Click);
            // 
            // lbl_UserName
            // 
            this.lbl_UserName.AutoSize = true;
            this.lbl_UserName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_UserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_UserName.ForeColor = System.Drawing.Color.FloralWhite;
            this.lbl_UserName.Location = new System.Drawing.Point(133, 384);
            this.lbl_UserName.Name = "lbl_UserName";
            this.lbl_UserName.Size = new System.Drawing.Size(94, 16);
            this.lbl_UserName.TabIndex = 7;
            this.lbl_UserName.Text = "User Name :";
            // 
            // lbl_Password
            // 
            this.lbl_Password.AutoSize = true;
            this.lbl_Password.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Password.ForeColor = System.Drawing.Color.FloralWhite;
            this.lbl_Password.Location = new System.Drawing.Point(133, 448);
            this.lbl_Password.Name = "lbl_Password";
            this.lbl_Password.Size = new System.Drawing.Size(84, 16);
            this.lbl_Password.TabIndex = 8;
            this.lbl_Password.Text = "Password :";
            // 
            // lbl_ConfirmPassword
            // 
            this.lbl_ConfirmPassword.AutoSize = true;
            this.lbl_ConfirmPassword.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ConfirmPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ConfirmPassword.ForeColor = System.Drawing.Color.FloralWhite;
            this.lbl_ConfirmPassword.Location = new System.Drawing.Point(406, 448);
            this.lbl_ConfirmPassword.Name = "lbl_ConfirmPassword";
            this.lbl_ConfirmPassword.Size = new System.Drawing.Size(146, 16);
            this.lbl_ConfirmPassword.TabIndex = 9;
            this.lbl_ConfirmPassword.Text = "Confirm Password* :";
            // 
            // lbl_Email
            // 
            this.lbl_Email.AutoSize = true;
            this.lbl_Email.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Email.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Email.ForeColor = System.Drawing.Color.FloralWhite;
            this.lbl_Email.Location = new System.Drawing.Point(409, 387);
            this.lbl_Email.Name = "lbl_Email";
            this.lbl_Email.Size = new System.Drawing.Size(74, 16);
            this.lbl_Email.TabIndex = 10;
            this.lbl_Email.Text = "Email ID :";
            // 
            // txt_UserName
            // 
            this.txt_UserName.Location = new System.Drawing.Point(249, 384);
            this.txt_UserName.Name = "txt_UserName";
            this.txt_UserName.Size = new System.Drawing.Size(116, 21);
            this.txt_UserName.TabIndex = 12;
            // 
            // txt_Password
            // 
            this.txt_Password.Location = new System.Drawing.Point(249, 442);
            this.txt_Password.Name = "txt_Password";
            this.txt_Password.Size = new System.Drawing.Size(116, 21);
            this.txt_Password.TabIndex = 13;
            // 
            // txt_NewPassword
            // 
            this.txt_NewPassword.Location = new System.Drawing.Point(550, 445);
            this.txt_NewPassword.Name = "txt_NewPassword";
            this.txt_NewPassword.Size = new System.Drawing.Size(116, 21);
            this.txt_NewPassword.TabIndex = 14;
            // 
            // txt_EmailId
            // 
            this.txt_EmailId.Location = new System.Drawing.Point(550, 387);
            this.txt_EmailId.Name = "txt_EmailId";
            this.txt_EmailId.Size = new System.Drawing.Size(116, 21);
            this.txt_EmailId.TabIndex = 15;
            // 
            // lblPriority
            // 
            this.lblPriority.AutoSize = true;
            this.lblPriority.BackColor = System.Drawing.Color.Transparent;
            this.lblPriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPriority.ForeColor = System.Drawing.Color.FloralWhite;
            this.lblPriority.Location = new System.Drawing.Point(308, 507);
            this.lblPriority.Name = "lblPriority";
            this.lblPriority.Size = new System.Drawing.Size(57, 16);
            this.lblPriority.TabIndex = 18;
            this.lblPriority.Text = "&Priority";
            // 
            // cbo_priority
            // 
            this.cbo_priority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_priority.FormattingEnabled = true;
            this.cbo_priority.Items.AddRange(new object[] {
            "admin",
            "user"});
            this.cbo_priority.Location = new System.Drawing.Point(393, 504);
            this.cbo_priority.Name = "cbo_priority";
            this.cbo_priority.Size = new System.Drawing.Size(116, 23);
            this.cbo_priority.TabIndex = 19;
            // 
            // dgv_accounts
            // 
            this.dgv_accounts.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_accounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_accounts.Location = new System.Drawing.Point(123, 103);
            this.dgv_accounts.Name = "dgv_accounts";
            this.dgv_accounts.Size = new System.Drawing.Size(543, 226);
            this.dgv_accounts.TabIndex = 20;
            this.dgv_accounts.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgv_accounts_MouseClick);
            // 
            // frmManageAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImage = global::Project1.Properties.Resources.image1112;
            this.ClientSize = new System.Drawing.Size(961, 600);
            this.Controls.Add(this.dgv_accounts);
            this.Controls.Add(this.cbo_priority);
            this.Controls.Add(this.lblPriority);
            this.Controls.Add(this.txt_EmailId);
            this.Controls.Add(this.txt_NewPassword);
            this.Controls.Add(this.txt_Password);
            this.Controls.Add(this.txt_UserName);
            this.Controls.Add(this.lbl_Email);
            this.Controls.Add(this.lbl_ConfirmPassword);
            this.Controls.Add(this.lbl_Password);
            this.Controls.Add(this.lbl_UserName);
            this.Controls.Add(this.btn_UpdateAccount);
            this.Controls.Add(this.btn_RemoveAccount);
            this.Controls.Add(this.btn_AddAccount);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmManageAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manage Account";
            this.Load += new System.EventHandler(this.frmManageAccount_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_accounts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_AddAccount;
        private System.Windows.Forms.Button btn_RemoveAccount;
        private System.Windows.Forms.Button btn_UpdateAccount;
        private System.Windows.Forms.Label lbl_UserName;
        private System.Windows.Forms.Label lbl_Password;
        private System.Windows.Forms.Label lbl_ConfirmPassword;
        private System.Windows.Forms.Label lbl_Email;
        private System.Windows.Forms.TextBox txt_UserName;
        private System.Windows.Forms.TextBox txt_Password;
        private System.Windows.Forms.TextBox txt_NewPassword;
        private System.Windows.Forms.TextBox txt_EmailId;
        private System.Windows.Forms.Label lblPriority;
        private System.Windows.Forms.ComboBox cbo_priority;
        private System.Windows.Forms.DataGridView dgv_accounts;
    }
}