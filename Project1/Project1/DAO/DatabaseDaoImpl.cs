﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using System.Windows.Forms;

namespace Project1
{
    public class DatabaseDaoImpl : DatabaseDao
    {
        public OracleConnection data_Conn()
        {
            OracleConnection conn = null;
            string strConnectionString =
                      "DATA SOURCE =dilbert.humber.ca:1521/grok;" + "USER ID= n01004222;PASSWORD= oracle";
            try
            {
                conn = new OracleConnection(strConnectionString);
                return conn;
            }
            catch(OracleException ex)
            {
                MessageBox.Show("Database Login Fail "+ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return conn;
        }

        /*count number of items in the table accourding to the sql passing to the function*/
        public int CountTableItems(string sql)
        {
            int count = 0;
            OracleConnection conn = null;
            try
            {
                DatabaseDaoImpl dbo = new DatabaseDaoImpl();
                conn = dbo.data_Conn();
                OracleCommand cmd = new OracleCommand(sql, conn);
                conn.Open();
                OracleDataReader reader = cmd.ExecuteReader();
                reader.Read();
                count = reader.GetInt32(0);
                conn.Close();
            }
            catch (OracleException ex)
            {
                //MessageBox.Show(ex.Message);

            }catch(Exception ex){
                //MessageBox.Show(ex.Message);
            }
            finally {
                conn.Close();
            }
            return count;
        }
    }
}
