﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using System.Windows.Forms;

namespace Project1
{
    class DatabaseConnection
    {


        private OracleConnection oracleConnection;
        private OracleCommand oracleCommand;
        private const string strCONNECTIONSTRING = "DATA SOURCE =dilbert.humber.ca:1521/grok; USER ID =n01004222; PASSWORD= oracle";


        public DatabaseConnection()
        {
            /*
            try
            {
                oracleConnection = new OracleConnection(strCONNECTIONSTRING);
                oracleCommand = new OracleCommand();
                oracleCommand.Connection = oracleConnection;
            }
            catch (Exception objException)
            {
                throw objException;
            }
            */
        }

        public int loadDataGridView(DataGridView dgv, string sql)
        {
            int i = 0;
            try
            {
                OracleConnection conn = new OracleConnection(strCONNECTIONSTRING);
                OracleCommand cmd = new OracleCommand(sql, conn);
                OracleDataAdapter adapter = new OracleDataAdapter(cmd);
                OracleCommandBuilder builder = new OracleCommandBuilder(adapter);
                DataTable dt = new DataTable();
                adapter.Update(dt);
                adapter.Fill(dt);
                dgv.DataSource = dt;
                i = 1;
            }
            catch (OracleException ex)
            {
               // MessageBox.Show(ex.Message);
            }
            return i;
        }

        public int addTableOrder(string sql)
        {
            DatabaseDaoImpl dbo = new DatabaseDaoImpl();
            OracleConnection oracleConnection = dbo.data_Conn();
            int check = 0;
            try
            {
                OracleCommand oracleCommand = new OracleCommand();
                oracleCommand.Connection = oracleConnection;
                oracleCommand.CommandText = sql;
                OracleDataReader oracleReader;
                oracleConnection.Open();
                oracleReader = oracleCommand.ExecuteReader();
                //MessageBox.Show("Table booked.");
                check = 1;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                oracleConnection.Close();
            }
            return check;
        }
    }
}
