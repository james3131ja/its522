﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MALUO824193130LAB4
{
    public partial class CustomerOrder : Form
    {
        
        SortedDictionary<string, double> dic = new SortedDictionary<string, double>();
        public CustomerOrder()
        {
            InitializeComponent();
        }

        public CustomerOrder(Customer o)
        {
            InitializeComponent();
            this.txtFnameOrder.Text=o.Fname;
            this.txtLnameOrder.Text=o.Lname;
            //this.f1 = f1;
        }

        private void btnBliling_Click(object sender, EventArgs e)
        {
            if (nupQuantity.Value!=0 && MyValidate.validate(txtFnameOrder) 
                && MyValidate.validate(txtLnameOrder)
                && MyValidate.validate(txtPrice))
            {
                //text is like $ 20
                Order o = new Order(Convert.ToDouble(txtPrice.Text.Split(new char[]{' '})[1]) * Convert.ToInt32(nupQuantity.Value));
                txtTotalOrder.Text = Convert.ToString(o.Total);
                o.Fname = txtFnameOrder.Text;
                o.Lname = txtLnameOrder.Text;
                CustomerBilling f4 = new CustomerBilling(o);
                f4.MdiParent = this.MdiParent;
                f4.Show();
            }
            else {
                MessageBox.Show("Not Validated Form!!!");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CustomerOrder_Load(object sender, EventArgs e)
        {
            dic.Add("Computer", 15);
            dic.Add("Laptop", 20);
            dic.Add("Hard Drive", 30);
            string[] strArray = new string[] {"Computer","Laptop","Hard Drive"};
            foreach (string i in strArray) {
                cboItem.Items.Add(i);
            }
        }

        private void cboItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            double price=0;
            dic.TryGetValue(cboItem.Text, out price);
            txtPrice.Text = "$ "+price.ToString();
        }
        
    }
}
