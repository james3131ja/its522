﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MALUO824193130LAB4
{
    public partial class CustomerInfo : Form
    {

      
        public CustomerInfo()
        {
            InitializeComponent();
        }

        public CustomerInfo(CustomerService f1)
        {
            InitializeComponent();
        }


        private void Form2_Load(object sender, EventArgs e)
        {
            tabPage1.Text = @"Personal Info";
            tabPage2.Text = @"Company";
        }

        private void btnOrder_Click(object sender, EventArgs e)
        {
            if (MyValidate.validate(txtFname) && MyValidate.validate(txtLname)
                && MyValidate.validate(txtPhone) && MyValidate.validate(txtEmail))
            {
                if (MyValidate.validateEmail(txtEmail))
                {
                    Customer c = new Customer(txtFname.Text, txtLname.Text);
                    CustomerOrder f3 = new CustomerOrder(c);
                    f3.MdiParent = this.MdiParent;
                    f3.Show();
                }
            }
        }

        private void txtFname_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
