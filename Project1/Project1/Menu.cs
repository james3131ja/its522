﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    public class Menu
    {
        private string name,description, category;
        private int item_id, comboNum,quantity;
        private double unit_price;
        public int Item_id { get { return item_id; } set { this.item_id = value; } }
        public string Name { get { return name; } set { name = value; } }
        public double UnitPrice { get { return unit_price; } set { unit_price = value; } }
        public string Desc { get { return description; } set { description = value; } }
        public string Category { get { return category; } set { category = value; } }
        public int ComboNum { get { return comboNum; } set { comboNum = value; } }
        public int Quantity { get { return quantity; } set { quantity = value; } }

        /*custructor*/
        public Menu(){
            Item_id = 0;
            Name = "TBD";
            UnitPrice = 0.0;
            Desc = "TBD";
            Category = "TBD";
            ComboNum = 0;
            Quantity = 0;
        }

        /*with out the description*/
        public Menu(int item_id,string name,int price,int quantity) {
            Item_id = item_id;
            Name = name;
            UnitPrice = price;
            Quantity = quantity;
        }

        /*with out the combo num and description*/
        public Menu(int item_id,string n,double u,string d) {
            Item_id = item_id;
            Name = n;
            UnitPrice = u;
            Desc = d;
            Quantity = 0;
        }

        /*all attributes are set - only if the item has to be set in the combo set and description has to be added*/
        public Menu(int item_id, string n, double u, string d,string category,int combo)
        {
            Item_id = item_id;
            Name = n;
            UnitPrice = u;
            Desc = d;
            Category = category;
            ComboNum = combo;
            Quantity = 0;
        }

        public override string ToString()
        {
            return " "+Name+" "+UnitPrice+" "+Desc+" "+Category;
        }

    }
}
