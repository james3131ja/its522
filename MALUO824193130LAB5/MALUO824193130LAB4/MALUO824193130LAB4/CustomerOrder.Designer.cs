﻿namespace MALUO824193130LAB4
{
    partial class CustomerOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBliling = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblFnamOrder = new System.Windows.Forms.Label();
            this.lblLnameOrder = new System.Windows.Forms.Label();
            this.lblItems = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.txtFnameOrder = new System.Windows.Forms.TextBox();
            this.txtLnameOrder = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.cboItem = new System.Windows.Forms.ComboBox();
            this.lblTotalOrder = new System.Windows.Forms.Label();
            this.txtTotalOrder = new System.Windows.Forms.TextBox();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.nupQuantity = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nupQuantity)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBliling
            // 
            this.btnBliling.Location = new System.Drawing.Point(27, 228);
            this.btnBliling.Name = "btnBliling";
            this.btnBliling.Size = new System.Drawing.Size(75, 23);
            this.btnBliling.TabIndex = 4;
            this.btnBliling.Text = "&Billing";
            this.btnBliling.UseVisualStyleBackColor = true;
            this.btnBliling.Click += new System.EventHandler(this.btnBliling_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(144, 228);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblFnamOrder
            // 
            this.lblFnamOrder.AutoSize = true;
            this.lblFnamOrder.Location = new System.Drawing.Point(24, 18);
            this.lblFnamOrder.Name = "lblFnamOrder";
            this.lblFnamOrder.Size = new System.Drawing.Size(57, 13);
            this.lblFnamOrder.TabIndex = 8;
            this.lblFnamOrder.Text = "&First Name";
            // 
            // lblLnameOrder
            // 
            this.lblLnameOrder.AutoSize = true;
            this.lblLnameOrder.Location = new System.Drawing.Point(24, 52);
            this.lblLnameOrder.Name = "lblLnameOrder";
            this.lblLnameOrder.Size = new System.Drawing.Size(58, 13);
            this.lblLnameOrder.TabIndex = 9;
            this.lblLnameOrder.Text = "&Last Name";
            // 
            // lblItems
            // 
            this.lblItems.AutoSize = true;
            this.lblItems.Location = new System.Drawing.Point(24, 87);
            this.lblItems.Name = "lblItems";
            this.lblItems.Size = new System.Drawing.Size(27, 13);
            this.lblItems.TabIndex = 10;
            this.lblItems.Text = "&Item";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(24, 122);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(31, 13);
            this.lblPrice.TabIndex = 11;
            this.lblPrice.Text = "&Price";
            // 
            // txtFnameOrder
            // 
            this.txtFnameOrder.Location = new System.Drawing.Point(116, 18);
            this.txtFnameOrder.Name = "txtFnameOrder";
            this.txtFnameOrder.Size = new System.Drawing.Size(100, 20);
            this.txtFnameOrder.TabIndex = 0;
            // 
            // txtLnameOrder
            // 
            this.txtLnameOrder.Location = new System.Drawing.Point(116, 52);
            this.txtLnameOrder.Name = "txtLnameOrder";
            this.txtLnameOrder.Size = new System.Drawing.Size(100, 20);
            this.txtLnameOrder.TabIndex = 1;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(116, 122);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(100, 20);
            this.txtPrice.TabIndex = 6;
            this.txtPrice.TabStop = false;
            // 
            // cboItem
            // 
            this.cboItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboItem.FormattingEnabled = true;
            this.cboItem.Location = new System.Drawing.Point(116, 87);
            this.cboItem.Name = "cboItem";
            this.cboItem.Size = new System.Drawing.Size(100, 21);
            this.cboItem.TabIndex = 2;
            this.cboItem.SelectedIndexChanged += new System.EventHandler(this.cboItem_SelectedIndexChanged);
            // 
            // lblTotalOrder
            // 
            this.lblTotalOrder.AutoSize = true;
            this.lblTotalOrder.Location = new System.Drawing.Point(24, 187);
            this.lblTotalOrder.Name = "lblTotalOrder";
            this.lblTotalOrder.Size = new System.Drawing.Size(31, 13);
            this.lblTotalOrder.TabIndex = 13;
            this.lblTotalOrder.Text = "&Total";
            // 
            // txtTotalOrder
            // 
            this.txtTotalOrder.Location = new System.Drawing.Point(116, 187);
            this.txtTotalOrder.Name = "txtTotalOrder";
            this.txtTotalOrder.ReadOnly = true;
            this.txtTotalOrder.Size = new System.Drawing.Size(100, 20);
            this.txtTotalOrder.TabIndex = 7;
            this.txtTotalOrder.TabStop = false;
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Location = new System.Drawing.Point(24, 156);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(46, 13);
            this.lblQuantity.TabIndex = 12;
            this.lblQuantity.Text = "&Quantity";
            // 
            // nupQuantity
            // 
            this.nupQuantity.Location = new System.Drawing.Point(116, 154);
            this.nupQuantity.Name = "nupQuantity";
            this.nupQuantity.Size = new System.Drawing.Size(100, 20);
            this.nupQuantity.TabIndex = 3;
            this.nupQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CustomerOrder
            // 
            this.AcceptButton = this.btnBliling;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(326, 354);
            this.Controls.Add(this.nupQuantity);
            this.Controls.Add(this.lblQuantity);
            this.Controls.Add(this.txtTotalOrder);
            this.Controls.Add(this.lblTotalOrder);
            this.Controls.Add(this.cboItem);
            this.Controls.Add(this.txtPrice);
            this.Controls.Add(this.txtLnameOrder);
            this.Controls.Add(this.txtFnameOrder);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.lblItems);
            this.Controls.Add(this.lblLnameOrder);
            this.Controls.Add(this.lblFnamOrder);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnBliling);
            this.Name = "CustomerOrder";
            this.Text = "Customer Orders";
            this.Load += new System.EventHandler(this.CustomerOrder_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nupQuantity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBliling;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblFnamOrder;
        private System.Windows.Forms.Label lblLnameOrder;
        private System.Windows.Forms.Label lblItems;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox txtFnameOrder;
        private System.Windows.Forms.TextBox txtLnameOrder;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.ComboBox cboItem;
        private System.Windows.Forms.Label lblTotalOrder;
        private System.Windows.Forms.TextBox txtTotalOrder;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.NumericUpDown nupQuantity;
    }
}