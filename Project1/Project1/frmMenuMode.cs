﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1
{
    public partial class frmMenuMode : Form
    {
        MenuForm menus = new MenuForm();

        public frmMenuMode()
        {
            InitializeComponent();
        }

        private void frmMenuMode_Load(object sender, EventArgs e)
        {
            lstMenuMode.FullRowSelect = true;
            load_ListToView();
            rdoAll.Checked = true;
        }   

        private void load_ListToView()
        {
            lstMenuMode = menus.loadMenuToList(lstMenuMode);
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            menus.MenuChecks(lstMenuMode);
        }   

        private void lstMenuMode_DoubleClick(object sender, EventArgs e)
        {
            menus.showDesc(lstMenuMode);
        }

        //quantity of an item is increased
        private void btnIncrease_Click(object sender, EventArgs e)
        {
            lstMenuMode = menus.quantityIncrease(lstMenuMode);
        }

        //quantity of an item is decreased
        private void btnDecrease_Click(object sender, EventArgs e)
        {
            lstMenuMode = menus.quantityDecrease(lstMenuMode);
        }

        //check and uncheck
        private void lstMenuMode_Click(object sender, EventArgs e)
        {
            try
            {
                int i = lstMenuMode.SelectedIndices[0];
                lstMenuMode.Items[i].Checked =
                    (lstMenuMode.Items[i].Checked == true) ? false : true;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        //close menu form
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        
        
        /*************************load by categories************************/
        private void rdoAll_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoAll.Checked)
            {
                lstMenuMode = menus.loadByCategory(lstMenuMode, rdoAll);
            }
        }
        
        private void rdoMains_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoMain.Checked)
            {
                lstMenuMode = menus.loadByCategory(lstMenuMode, rdoMain);
            }
        }

        private void rdoAppetizer_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoAppetizer.Checked)
            {
                lstMenuMode = menus.loadByCategory(lstMenuMode, rdoAppetizer);
            }
        }

        private void rdoBeverages_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoBeverage.Checked)
            {
                lstMenuMode = menus.loadByCategory(lstMenuMode, rdoBeverage);
            }
        }

        private void rdoDesserts_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoDessert.Checked)
            {
                lstMenuMode = menus.loadByCategory(lstMenuMode, rdoDessert);
            }
        }
    }
}
