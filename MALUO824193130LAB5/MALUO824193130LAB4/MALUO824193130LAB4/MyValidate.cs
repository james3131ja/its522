﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace MALUO824193130LAB4
{
    public class MyValidate
    {

        public static bool validate(TextBox o) {
            bool b=false;
            if (o.Text == "")
            {
                MessageBox.Show(o.Name + "Not Filled");
            }
            else {
                b = true;
            }
            return b;
        }

        public static bool validateEmail(TextBox o)
        {
            bool b = false;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(o.Text);
            if (!match.Success) { MessageBox.Show("Not Validated Email"); }
            else
            {
                MessageBox.Show("Pass!!!"); b = true;
            }
            return b;
        }
    }
}
